@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Edit Scores</b></div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('update_score') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$score->id}}">
                    <input type="hidden" name="class_id" value="{{$class_id}}">
                    <input type="submit" class="btn btn-primary float-right" value="Update Score">
                    <a href="/score/delete/{{$score->id}}?C={{$class_id}}" class="btn btn-danger">Delete Score</a>
                    <a href="/classes/detail/{{$class_id}}" class="btn btn-secondary ">Cancel and Return to Class</a>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Base Score</b></label>
                        <input type="number" class="form-control" name="base_score" value="{{$score->base_score}}"  required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Raw Score</b></label>
                        <input type="number" class="form-control" name="raw_score" value="{{$score->raw_score}}" placeholder="Input Raw Score" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect2"><b>Score Type</b></label>
                        <select class="form-control" name="type" required>
                            <option value="{{$score->type}}" selected>{{$score->type}}</option>
                            <option value="Firearms" selected>Firearms</option>
                            <option value="Written Test">Written Test</option>
                            <option value="Practical">Practical</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><b>Pass or Fail</b></label>
                        <select class="form-control" name="fail">
                            <option value="{{$score->fail}}" selected>{{$score->fail}}</option>
                            <option value="Pass" >Pass</option>
                            <option value="Fail">Fail</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Course</b></label>
                        <input type="text" class="form-control" name="course"  value="{{$score->course}}" value="Pistol Qualification" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Firearm Make</b></label>
                        <input type="text" class="form-control" name="firearm_make"  value="{{$score->firearms_make}}" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Firearm Model</b></label>
                        <input type="text" class="form-control" name="firearm_model"  value="{{$score->firearm_model}}" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Firearm Serial Number</b></label>
                        <input type="text" class="form-control" name="firearm_serial" value="{{$score->firearm_serial}}" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Score Notes</b></label>
                        <textarea class="form-control" name="score_notes" rows="3">{{$score->score_notes}}</textarea>
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="Update Score">
                    <a href="/classes/detail/{{$class_id}}" class="btn btn-secondary ">Cancel and Return to Class</a>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

        $(document).ready(function() {
            $('.students').select2({
                placeholder: "Select student",
                allowClear: true
            });
        });

    </script>
@endsection
