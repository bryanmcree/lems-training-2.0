@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Add Agency</b></div>

            <div class="card-body">
                <form method="POST" action="{{ route('store_agency') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Agency Name</b></label>
                        <input type="text" class="form-control" name="agency_name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name of Department or Organization" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Agency Administrator</b></label>
                        <select class="form-control users" name="agency_admin" required>
                            <option>[Select Administrator]</option>
                            @foreach($users as $user)
                                <option value="{{$user->account_number}}">{{$user->last_name}}, {{$user->first_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Agency ORI</b></label>
                        <input type="text" class="form-control" name="agency_ori" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Or other Agency ID" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Address</b></label>
                        <input type="text" class="form-control" name="address" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Primary Street Address" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Address</b></label>
                        <input type="text" class="form-control" name="address1" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Suite C5">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>City</b></label>
                        <input type="text" class="form-control" name="city" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="City" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>State</b></label>
                        <select class="form-control states" name="state" >
                            <option>[Select State]</option>
                            @foreach($states as $state)
                                <option value="{{$state->code}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Zip</b></label>
                        <input type="text" class="form-control" name="zip" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Zip Code" >
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="Add Agency">
                    <input type="reset" class="btn btn-secondary">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.states').select2();
        });

        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.users').select2();
        });

    </script>
@endsection
