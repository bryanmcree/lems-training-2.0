@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>{{$class->class_name}}</b></div>
            <form method="POST" action="{{ route('roster_manual_add') }}">
                @csrf
                <div class="card-body">
                    <input type="hidden" name="class_id" value="{{$class->id}}">
                    <input type="hidden" name="signature" id="offsignature" required>
                    <input type="hidden" name="barcode" value="{{$barcode}}">
                    <div class="card-body">
                        <div class="form-group">
                            <i>Email and phone will only be used in the event there is an issue awarding POST credit.  If not provided it will be the students responsibility to ensure Okey and name match POST records.</i>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>First Name</b></label>
                            <input type="text" class="form-control" name="first_name" id="exampleInputEmail1" aria-describedby="emailHelp" >
                            <small id="emailHelp" class="form-text text-muted">Please use the first name registered with Georgia POST.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Last Name</b></label>
                            <input type="text" class="form-control" name="last_name" id="exampleInputEmail1" aria-describedby="emailHelp" >
                            <small id="emailHelp" class="form-text text-muted">Please use the last name registered with Georgia POST.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Suffix</b></label>
                            <select class="form-control" id="suffix" name="suffix">
                                <option value="" selected="selected">[None]</option>
                                <option value="Jr.">Jr.</option>
                                <option value="Sr.">Sr.</option>
                                <option value="III">III</option>
                                <option value="IV">IV</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Email</b></label>
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" >
                            <small id="emailHelp" class="form-text text-muted">Enter valid email address.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Phone Number</b></label>
                            <input autocomplete="asdfasdf" type="text" class="form-control" name="phone"  id="student_phone"  required>
                            <small id="emailHelp" class="form-text text-muted">REQUIRED - Enter phone number in the event there is an issue with post.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Okey</b></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">O</span>
                                </div>
                                <input type="number" class="form-control" name="officer_key_number" id="exampleInputEmail1" aria-describedby="emailHelp" >
                            </div>

                            <small id="emailHelp" class="form-text text-muted">*Numbers ONLY. EVERYTHING AFTER THE LETTER O.  Not correctly entering your OKey number could delay getting POST credit.</small>
                        </div>
                    </div>
                </div>
                <div class="card-header">
                    <b>Sign Below</b>
                </div>
                <div class="card-body">
                    <div id="signature"></div>
                    <button type="button" class="btn btn-block btn-secondary" onclick="$('#signature').jSignature('clear')">Clear Signature</button>
                </div>
                <div class="card-footer">
                    <input type="submit" id="btnSave" value="Add Student, Register and Return to Roster" class="btn btn-block btn-primary ">
                </div>
            </form>
        </div>
    </div>
@endsection
<script src="{{ asset('js/inputmask.js') }}" defer></script>
@section('scripts')
    <script type="application/javascript">
        $(document).ready(function() {
            $("#signature").jSignature({
                color: "blue",
                lineWidth: 1,
                showLine: true
            })
        });

        $('#btnSave').click(function(){
            var sigData = $('#signature').jSignature('getData','svgbase64');
            $('#offsignature').val(sigData);

        });


        $(document).ready(function () {
            $('#inventory').DataTable();
            $("#student_phone").inputmask({"mask": "(999) 999-9999"});
            $("#transfer_cell_phone").inputmask({"mask": "(999) 999-9999"});
            $("#dealer_phone").inputmask({"mask": "(999) 999-9999"});
            $("#dealer_mobile").inputmask({"mask": "(999) 999-9999"});
            $("#dealer_ffl_number").inputmask({"mask": "9-99-999-99-9A-99999"});
        });
    </script>
@endsection
