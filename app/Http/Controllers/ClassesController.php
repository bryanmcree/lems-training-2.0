<?php

namespace App\Http\Controllers;

use App\Classes;
use App\ClassStudentPiviot;
use App\Exports\StudentRoster;
use App\History;
use App\Http\Requests\SignatureRequest;
use App\Students;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class ClassesController extends Controller
{


    public function index(){
        $classes = Classes::where('account_number','=', Auth::user()->account_number)
           // ->orderby('completed','desc')
            ->orderBy('created_at','desc')

            ->get();
        //dd($classes);
        return View('classes.index', compact('classes'));
    }

    public function index_admin($id){
        $classes = Classes::where('account_number','=', $id)
            ->orderBy('start_date','desc')
            ->orderby('completed')
            ->get();
        return View('classes.index', compact('classes'));
    }

    public function detail($id){
        $class = Classes::where('id','=', $id)
            ->where('account_number','=', Auth::user()->account_number)
            ->first();
        return View('classes.detail', compact('class'));
    }

    public function add_class(){
        $classes = Classes::select('course_id','class_name','studentsnotes')
            ->distinct()
            ->orderby('class_name')
            ->get();
        return View('classes.add_class', compact('classes'));
    }

    public function new_class(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('account_number', Auth::user()->account_number);
        $request->offsetSet('completed', 'No');
        $input = $request->all();
        Classes::create($input);
        alert()->success('Class has been added.', 'Class Added');

        $history = new History();
        $history->function = 'Created Class';
        $history->username = \Auth::user()->email;
        $history->notes = 'Created ' . $request->class_name;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->save();

        return redirect('/classes');
    }

    public function studentsSignature($id)
    {
        $student = Students::where('id','=',Input::get('S'))
            ->first();
        //dd($student);
        $class = Classes::where('account_number','=', Auth::user()->account_number)
            ->where('id','=',$id)
            ->first();
        return view('classes.signature', compact('class','student'));
    }

    public function storeSignature(Request $request)
    {
        $updateTbl = DB::table('classes_students')
            ->where('students_id','=',$request->students_id)
            ->where('classes_id','=',$request->classes_id)
            ->update([
                'signature'=>$request->signature,
                'signature_date'=>Carbon::now()
            ]);
        alert()->success('Signature has been updated!', 'Success!');
        return redirect('/classes/detail/'.$request->classes_id);
    }

    public function searchClasses(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = Classes::select('course_id','class_name')
            ->where('class_name', 'LIKE', '%' . $term . '%')
            ->limit(5)
            ->distinct()
            ->get();
        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->course_id, 'text' => $tag->class_name];
        }
        return \Response::json($formatted_tags);
    }

    //This adds from the class detail
    public function addStudent(Request $request)
    {
        $input = $request->except(['search']);
        ClassStudentPiviot::create($input);
        alert()->success('Student has been added.', 'Student Added');
        return redirect('/classes/detail/'.$request->classes_id);
    }

    //this one adds from the student ROSTER
    public function addStudent_roster(SignatureRequest $request)
    {

        //dd($request);
        //make sure student is not already signed up.
        $check = ClassStudentPiviot::where('students_id','=', $request->student_id)
            ->where('classes_id','=', $request->class_id)
            ->first();

        if($request->barcode == 1){
            if(is_null($check)){
                //now lets add student to the class with their signature
                $enroll = new ClassStudentPiviot();
                $enroll->students_id = $request->student_id;
                $enroll->classes_id = $request->class_id;
                $enroll->signature = $request->signature;
                $enroll->signature_date = Carbon::now();
                $enroll->save();
                return view('barcode.confirm_enroll', compact('class_id','student'));
            }else{
                return view('barcode.confirm_enroll', compact('class_id','student'));
            }
        }

        if(is_null($check)){
            //now lets add student to the class with their signature
            $enroll = new ClassStudentPiviot();
            $enroll->students_id = $request->student_id;
            $enroll->classes_id = $request->class_id;
            $enroll->signature = $request->signature;
            $enroll->signature_date = Carbon::now();
            $enroll->save();
            alert()->success('Student has been added.', 'Student Added');
            return redirect('/classes/signin/'.$request->class_id);
        }else{
            alert()->success('You are already signed up for this class!', 'NOTICE')->persistent("Oops, Got it!");
            return redirect('/classes/signin/'.$request->class_id);
        }
    }

    public function edit_student($id){
        $student = Students::where('id','=', Input::get('S'))
            ->first();
        $class_id = $id;
        return view('classes.edit_student', compact('class_id','student'));
    }

    public function update_student(){

        if(is_null(Input::get('class_id'))){
            $student = Students::find(Input::get('student_id'));
            $student->update(Input::except('student_id'));
            $history = new History();
            $history->function = 'Updated Student';
            $history->username = \Auth::user()->email;
            $history->notes = 'Updated ' . $student->last_name .', '. $student->first_name . ' ID' . $student->id;
            $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
            $history->save();
            alert()->success('Student has been Updated!', 'Success!');
            return redirect('/students/');
        }else{
            $student = Students::find(Input::get('student_id'));
            $student->update(Input::except('class_id','student_id'));
            $history = new History();
            $history->function = 'Updated Student';
            $history->username = \Auth::user()->email;
            $history->notes = 'Updated ' . $student->last_name .', '. $student->first_name . ' ID' . $student->id;
            $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
            $history->save();
            alert()->success('Student has been Updated!', 'Success!');
            return redirect('/classes/detail/' . Input::get('class_id'));
        }
    }

    public function manual_student($id){

        $class_id = $id;
        return view('classes.manual_add_student', compact('class_id'));
    }

    public function manual_student_add(Request $request){

        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('account_number', Auth::user()->account_number);
        $input = $request->except(['class_id']);
        Students::create($input);

        $input2 = new ClassStudentPiviot;
        $input2->students_id = $request->id;
        $input2->classes_id = $request->class_id;
        $input2->save();
        alert()->success('Student has been added.', 'Student Added');
        return redirect('/classes/detail/'.$request->class_id);
    }

    public function postExport($id)
    {
        return \Maatwebsite\Excel\Facades\Excel::download(new StudentRoster($id), 'Student_Import' . date('m-d-Y_hia') .'.csv');
    }

    public function removeStudent($class_id, $students_id)
    {
        $class = ClassStudentPiviot::where('classes_id','=', $class_id)->where("students_id",'=', $students_id)->delete();

        $history = new History();
        $history->function = 'Remove Student';
        $history->username = \Auth::user()->email;
        $history->notes = 'Remove Student ClassID ' . $class_id .' StudentID'. $students_id;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->save();

        return redirect('/classes/detail/'.$class_id);
    }

    public function sign_in($id){
        $class = Classes::find($id);
        $barcode = 0;
        return view('classes.sign_in', compact('class','barcode'));
    }

    public function sign_in_studentsSignature($id)
    {
        $student = Students::where('account_number','=', Auth::user()->account_number)
            ->where('id','=',Input::get('S'))
            ->first();


        $class = Classes::where('account_number','=', Auth::user()->account_number)
            ->where('id','=',$id)
            ->first();

        return view('classes.sign_in_signature', compact('class','student'));
    }

    public function sign_in_storeSignature(Request $request)
    {
        $updateTbl = DB::table('classes_students')
            ->where('students_id','=',$request->students_id)
            ->where('classes_id','=',$request->classes_id)
            ->update([
                'signature'=>$request->signature,
                'signature_date'=>Carbon::now()
            ]);
        alert()->success('Signature has been updated!', 'Success!');
        return redirect('/classes/detail/'.$request->classes_id);
    }

    public function update_class()
    {
        $class = Classes::find(Input::get('id'));
        $class->update(Input::all());
        alert()->success('Class has been updated!', 'Success!');
        return redirect('/classes/detail/'. Input::get('id'));
    }

    public function barcode_signin($id){
        $class = Classes::find($id);
        return view('classes.barcode', compact('class'));
    }

    public function barcode_sign_in($id){
        $class = Classes::find($id);
        //dd($class);

        If($class->barcode_on == 0){
            return view('barcode.barcode_off', compact('class'));
        }else{
            $barcode = 1;
            return view('classes.sign_in', compact('barcode','class'));
        }

    }

    public function roster_manual($id){
        $class = Classes::find($id);
        $barcode = Input::get('barcode');

        return view('classes.roster_manual', compact('class','barcode'));
    }

    public function roster_manual_add(Request $request){


        //dd($request);
        //make sure that okey is not already being used.
        $check = Students::where('officer_key_number','=', $request->officer_key_number)
            ->first();

        if(is_null($check)){
            //now lets add student to the class with their signature
            $student_id = Uuid::generate(4);

            //Add new student to DB
            $student = new Students();
            $student->id = $student_id;

            if(!Auth::user()){
                $student->account_number = 'Manual Barcode Add';
            }else{
                $student->account_number = Auth::user()->account_number;
            }


            $student->first_name = $request->first_name;
            $student->last_name = $request->last_name;
            $student->suffix = $request->suffix;
            $student->phone = $request->phone;
            $student->email = $request->email;
            $student->officer_key_number = 'O' . $request->officer_key_number;
            $student->save();

            //now lets add student to the class with their signature
            $enroll = new ClassStudentPiviot();
            $enroll->students_id = $student_id;
            $enroll->classes_id = $request->class_id;
            $enroll->signature = $request->signature;
            $enroll->signature_date = Carbon::now();
            $enroll->save();
            alert()->success('Student has been added.', 'Student Added');

            if($request->barcode == 1){
                return view('barcode.confirm_enroll');
            }else{
                return redirect('/classes/signin/'.$request->class_id);
            }

        }else{
            alert()->success('There is already a student using that Okey number!', 'NOTICE')->persistent("Oops, Got it!");

            if($request->barcode == 1){
                return redirect('barcode.confirm_enroll');
            }else{
                return redirect('/classes/signin/'.$request->class_id);
            }
        }
    }

    public function print_pdf($id){

        $data = Classes::find($id);
        //dd($class);
        return view('pdf_views.print_class', compact('data'));
    }

    public function delete_class($id)
    {
        Classes::find($id)->delete();

        $history = new History();
        $history->function = 'Delete Class';
        $history->username = \Auth::user()->email;
        $history->notes = 'Deleted ClassID ' . $id;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->save();

        alert()->success('Class has been deleted.', 'Class Deleted');
        return redirect('/classes');
    }

}
