@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <form method="POST" action="{{ route('addfile') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="class_id" value="{{$class->id}}">
                <div class="card-header"><b>Add Document</b></div>

                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1"><b>Description</b></label>
                        <input type="text" class="form-control" name="file_description" placeholder="File Description" required>
                        <small id="emailHelp" class="form-text text-muted">Example could be "Lesson Plan" or "handouts".</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1"><b>Description</b></label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file_name">
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="Add Document">
                    <a href="/classes/detail/{{$class->id}}" class="btn btn-secondary">Cancel and Return to Class</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
