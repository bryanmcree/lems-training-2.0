@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Scores - {{$student->last_name}}, {{$student->first_name}}</b>
            @if(is_null($class_id))
                    <a href="/students" class="btn btn-primary float-right">Back to Students</a>
                @else
                    <a href="/classes/detail/{{$class_id}}" class="btn btn-primary float-right">Back to Class</a>
            @endif

            </div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card-deck">
                    <div class="card">
                        <div class="card-header">
                            <b>Firearm Scores Over Time</b>
                            <span class="float-right"><b><span  class="d-none d-lg-table-cell">Student Average</span> {{number_format($student->scores->avg('score_percent'),2)}}%</b></span>
                        </div>
                        <div class="card-body">
                            <canvas id="line-chart" width="800" height="350"></canvas>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <b>All Scores Over Time</b>
                        </div>
                        <div class="card-body">
                            <canvas id="all-scores" width="800" height="350"></canvas>
                        </div>
                    </div>
                </div>

                <table class="table" id="scores">
                    <thead>
                        <tr>
                            <td class="d-none d-lg-table-cell"><b>Type</b></td>
                            <td><b>Course</b></td>
                            <td align="center"><b>P/F</b></td>
                            <td align="center"><b>Raw</b></td>
                            <td align="center"><b>Percent</b></td>
                            <td><b>Make</b></td>
                            <td class="d-none d-lg-table-cell"><b>Model</b></td>
                            <td class="d-none d-lg-table-cell"><b>Serial</b></td>
                            <td class="d-none d-lg-table-cell" align="center"><b>Created Date</b></td>
                            <td class="d-none d-lg-table-cell" align="center"><b>Updated Date</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($student->scoresDes as $scores)
                        <tr>
                            <td class="d-none d-lg-table-cell">{{$scores->type}}</td>
                            <td>{{$scores->course}}</td>
                            <td align="center">{{$scores->fail}}</td>
                            <td align="center">{{$scores->raw_score}}/{{$scores->base_score}}</td>
                            <td align="center">{{$scores->score_percent}}%</td>
                            <td>{{$scores->firearm_make}}</td>
                            <td class="d-none d-lg-table-cell">{{$scores->firearm_model}}</td>
                            <td class="d-none d-lg-table-cell">{{$scores->firearm_serial}}</td>
                            <td class="d-none d-lg-table-cell" align="center">{{$scores->created_at->format('m-d-Y H:i')}}</td>
                            <td class="d-none d-lg-table-cell" align="center">{{$scores->updated_at->format('m-d-Y H:i')}}</td>
                            <td nowrap="" align="right"><i class="fad fa-pen-square"></i> <i class="fad fa-trash-alt"></i></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

        oTable = $('#scores').DataTable({
            bFilter: true,
            ordering: false,
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            dom: 'ltipr'         // This shows just the table
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said

        //Firearms Scores Over Time
        new Chart(document.getElementById("line-chart"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($student->scores->where('type','Firearms') as $scores_chart)
                    '{{$scores_chart->created_at->format('m-d-Y')}}',
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($student->scores->where('type','Firearms') as $scores_chart)
                        {{$scores_chart->score_percent}},
                        @endforeach
                    ],
                    label: "Firearm Scores",
                    borderColor: "#3e95cd",
                    fill: true
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                scales:{
                    xAxes: [{
                        display: false //this will remove all the x-axis grid lines
                    }]
                }
            }
        });

        //All Scores Over Time
        new Chart(document.getElementById("all-scores"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($student->scores as $scores_chart)
                        '{{$scores_chart->created_at->format('m-d-Y')}}',
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($student->scores as $scores_chart)
                        {{$scores_chart->score_percent}},
                        @endforeach
                    ],
                    label: "All Scores",
                    borderColor: "#cd3c3c",
                    fill: true
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                scales:{
                    xAxes: [{
                        display: false //this will remove all the x-axis grid lines
                    }]
                }
            }
        });

    </script>
@endsection
