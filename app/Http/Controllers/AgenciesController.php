<?php

namespace App\Http\Controllers;

use App\Agencies;
use App\States;
use App\User;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class AgenciesController extends Controller
{
    public function index()
    {
        $agencies = Agencies::orderBy('agency_name')->get();
        return View('agencies.index', compact('agencies'));
    }

    public function add()
    {
        $states = States::all();
        $users = User::all();
        return View('agencies.add', compact('states','users'));
    }

    public function store_agency(Request $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        Agencies::create($input);
        alert()->success('Agency has been added.', 'Agency Added');
        return redirect('/agencies');
    }
}
