<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'LEMSt') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="/css/font.css">

    <!-- Styles -->
    <link href="/css/dataTables.bootstrap.css">
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/css/select2.min.css" rel="stylesheet" />
    <script src="/js/fontawesome.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146443861-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146443861-1');
    </script>

</head>
<body>
    <div id="app">
        @auth
            @can('instructors_only')
                <!-- The Current User Can Update The Post -->
                    <main class="">
                        @include('nav.navbar')
                        <div class="container-fluid" >
                            @yield('content')
                        </div>
                    </main>
            @else
                <!-- The Current User Can Create New Post -->
                    <main class="">
                        @include('nav.navbar')
                        <div class="container-fluid" >
                            @include('not_verified')
                        </div>
                    </main>
            @endcan
        @else
                    <main class="">
                        @include('nav.navbar')
                        <div class="container-fluid" >
                            @yield('content')
                        </div>
                    </main>
        @endauth


    </div>
    <script src="/js/jquery.js"></script>
    <script src="/js/dataTables.js"></script>
    <script src="/js/popper.js"></script>
    <script src="/js/dataTables.bootstrap.js"></script>
    <script src="/js/jSignature.min.noconflict.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/select2.js"></script>
    <script src="/js/sweetalert.js"></script>
    <script src="/js/chart.js"></script>
    @include('sweet::alert')

    <script type="application/javascript">
        var input = document.getElementById('phone');
        input.onkeypress = function(){
            input.value = input.value.replace(/[^0-9+]/g, '');
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>


    @yield('scripts')

</body>
</html>
