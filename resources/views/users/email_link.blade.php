@extends('layouts.app')

@section('content')
    @if ( Auth::user()->hasRole('Administrator'))
        <div class="col-md-12">
            <div class="card mt-2">
                <div class="card-header"><b>Email Registration Link</b></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('send_link_email') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Email address</b></label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">Enter the email address of the person you would like to invite to LEMSt.  Remember once this person registers you will have to update their access.</small>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Send Link">
                    </form>
                </div>
            </div>
        </div>
    @else
        @include('unauthorized')
    @endif
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
