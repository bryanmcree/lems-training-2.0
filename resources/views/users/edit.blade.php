@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Edit User</b></div>

            <form method="POST" action="{{ route('update_user') }}">
                @csrf
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="card-footer">
                    <a href="/users" class="btn btn-secondary">Cancel and Return to Users</a>
                    <input type="submit" value="Update User" class="btn btn-primary float-right">
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>User Type</b></label>
                        <select class="form-control" id="suffix" name="user_type">
                            <option value="{{$user->user_type}}" selected="selected">{{$user->user_type}}</option>
                            <option value="Admin">Admin - Total God Privileges</option>
                            <option value="Instructor">Instructor - Add classes do instructor stuff</option>
                            <option value="Student">Student - See transcripts, sign up for classes</option>
                            <option value="Agency Admin">Agency Admin - User can add and delete other users to their agency.</option>
                            <option value="Disabled">Disabled - Removes all rights</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>First Name</b></label>
                        <input type="text" class="form-control" name="first_name" id="exampleInputEmail1" value ='{{$user->first_name}}' >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Last Name</b></label>
                        <input type="text" class="form-control" name="last_name" id="exampleInputEmail1" value ='{{$user->last_name}}' >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Suffix</b></label>
                        <select class="form-control" id="suffix" name="suffix">
                            <option value="{{$user->suffix}}" selected="selected">{{$user->suffix}}</option>
                            <option value="Jr.">Jr.</option>
                            <option value="Sr.">Sr.</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Email</b></label>
                        <input type="email" class="form-control" name="email" id="exampleInputEmail1" value ='{{$user->email}}' >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Phone Number</b></label>
                        <input type="text" class="form-control" name="phone"  id="phone" maxlength="10" minlength="10" value ='{{$user->phone}}' >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Okey</b></label>
                        <input type="text" class="form-control" name="officer_key_number" id="exampleInputEmail1" value ='{{$user->officer_key_number}}' >
                    </div>
                </div>
                <div class="card-footer">
                    <a href="/users" class="btn btn-secondary">Cancel and Return to Users</a>
                    <input type="submit" value="Update User" class="btn btn-primary float-right">
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
