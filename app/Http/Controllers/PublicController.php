<?php

namespace App\Http\Controllers;

use App\Classes;
use App\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class PublicController extends Controller
{

    public function contact_us(){

        return view('contact_us');
    }

    public function barcode_sign_in($id){
        $class = Classes::find($id);
        //dd($class);
        If($class->barcode_on == 0){
            $barcode = 0;
            return view('barcode.barcode_off', compact('class','barcode'));
        }else{
            $barcode = 1;
            return view('classes.sign_in', compact('barcode','class'));
        }
    }

    public function contact_us_submit(Request $request){

        //$request->offsetSet('id', Uuid::generate(4));

        $input = $request->except('g-recaptcha-response');

        ContactUs::create($input);

        //Lets send an email to the admin.
        Mail::send('emails.contact_us',array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone'),
            'ga_post' => $request->get('ga_post'),
            'ip_address' => $request->get('ip_address'),
            'comments' => $request->get('questions')), function ($m) use ($input){
            $m->replyTo($input['email']);
            $m->to('bryanmcree@gmail.com');
            $m->subject('LEMSt Contact Us');
        });

        Session::flash('alert-success', 'Your request has been sent!');
        return redirect('/contact_us');
    }

    public function public_classes(){

        $classes = Classes::where('class_public', 1)
            ->where('start_date','>', Carbon::now())
            ->orderby('start_date')
            ->get();

        return view('classes.public_classes', compact('classes'));
    }

    public function contact_instructor($id){
        $class = Classes::find($id);
        return view('classes.contact_instructor', compact('class'));

    }

    public function contact_instructor_submit(Request $request){

        $class = Classes::where('id', $request->class_id)
            ->first();

        $input = $request->except('g-recaptcha-response');

        //Lets send an email to the admin.
        Mail::send('emails.contact_instructor',array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),

            'start_date' => $class->start_date,
            'class_name' => $class->class_name,

            'phone_number' => $request->get('phone'),
            'ga_post' => $request->get('ga_post'),
            'ip_address' => $request->get('ip_address'),
            'officer_key_number' => $request->get('officer_key_number'),
            'comments' => $request->get('questions')), function ($m) use ($input, $class){
            //$m->from('no-reply@mysgmcbenefits.com', 'My SGMC Benefits');
            $m->replyTo($input['email']);
            $m->to($class->instructor->email);
            //$m->CC('bryan.mcree@sgmc.org');
            $m->subject('LEMSt Contact Us');
            //$m->CC('sghelpdesk@sgmc.org');
            //$m->CC($data['mail']);
            //$m->replyTo($data['mail']);
        });

        Session::flash('alert-success', 'Your request has been sent! ');
        return redirect('/classes/public');
    }
}
