<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agencies extends Model
{
    use SoftDeletes;
    protected $table = 'agencies';
    public $incrementing = false;
    protected $guarded = [];

    public function AgencyAdmin(){
        return $this->hasOne('\App\User','account_number','agency_admin');
    }
}
