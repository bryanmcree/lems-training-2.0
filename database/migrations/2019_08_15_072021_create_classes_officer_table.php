<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesOfficerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes_students', function (Blueprint $table) {
            $table->primary(['students_id', 'classes_id']);
            $table->string('students_id', 36)->index();
            $table->string('classes_id', 36)->index();
            $table->text('signature')->nullable(); //Student's signature for this class.
            $table->string('pass')->nullable(); //Did the student pass or fail this class?
            $table->text('student_notes')->nullable(); //Notes unique to this student in this class.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes_officer');
    }
}
