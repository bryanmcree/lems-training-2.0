<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassStudentPiviot extends Model
{
    protected $table = 'classes_students';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    protected $guarded = [];
    protected $dates = ['signature_date'];
}
