@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="card mt-2">
        <div class="card-header"><b>Add Student</b>
        </div>
        <form method="POST" action="{{ route('add_student') }}">
            @csrf
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group">
                    <label for="exampleInputEmail1"><b>First Name</b></label>
                    <input type="text" value="{{ old('first_name') }}" class="form-control" name="first_name" id="exampleInputEmail1" aria-describedby="emailHelp" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"><b>Last Name</b></label>
                    <input type="text" value="{{ old('last_name') }}" class="form-control" name="last_name" id="exampleInputEmail1" aria-describedby="emailHelp" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"><b>Suffix</b></label>
                    <select class="form-control" id="suffix" name="suffix">
                        <option  value="{{ old('suffix') }}" selected="selected"> {{ old('suffix') }}</option>
                        <option value="Jr.">Jr.</option>
                        <option value="Sr.">Sr.</option>
                        <option value="III">III</option>
                        <option value="IV">IV</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"><b>Email</b></label>
                    <input type="email" value="{{ old('email') }}" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"><b>Phone Number</b></label>
                    <input type="text"  value="{{ old('phone') }}" class="form-control" name="phone"  id="phone" maxlength="10" minlength="10" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1"><b>Okey</b></label>
                    <input type="text" value="{{ old('officer_key_number') }}" class="form-control" name="officer_key_number" id="exampleInputEmail1" aria-describedby="emailHelp" >
                </div>
                    @error('officer_key_number')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
            </div>
            <div class="card-footer">
                <a href="/students/" class="btn btn-secondary">Cancel and Return to Students</a>
                <input type="submit" value="Add Student" class="btn btn-primary float-right">
            </div>
    </div>
    </form>
</div>
@endsection

@section('scripts')
<script type="application/javascript">

</script>
@endsection
