@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header">
                <h3><b>Class Name</b></h3>
            </div>
            <div class="card-body">
                <h3>Scan code to sign in!</h3>
                {!! QrCode::size(600)->generate('lemsweb.com'); !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
