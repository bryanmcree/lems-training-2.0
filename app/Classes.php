<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classes extends Model
{
    use SoftDeletes;
    protected $table = 'classes';
    public $incrementing = false;
    protected $guarded = [];
    protected $dates = ['start_date'];


    public function studentsClass(){
        return $this->belongsToMany('\App\Students','classes_students')->withPivot('signature','signature_date')->orderby('last_name');
    }

    public function documentsClass(){
        return $this->hasMany('\App\Documents','class_id','id');
    }

    public function studentScores(){
        return $this->hasMany('\App\Scores','class_id','id');
    }

    public function instructor(){
        return $this->hasOne('\App\User','account_number','account_number');
    }
}
