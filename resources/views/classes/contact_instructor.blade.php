@extends('layouts.public')

@section('content')

    <div class="col-md-4 mx-auto ">
        <div class="card mt-2">
            <div class="card-header"><b>Contact Instructor</b>
            </div>

            <div class="card-body">

                <p>For more information regarding {{$class->class_name}} please fill out the form below.</p>
                <form method="POST" action="{{ route('contact_instructor_submit') }}">
                    @csrf
                    <input type="hidden" name="ip_address" value="{{request()->ip()}}">
                    <input type="hidden" name="class_id" value="{{$class->id}}">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Name</b></label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Email Address</b></label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Phone Number</b></label>
                        <input type="text"  value="{{ old('phone') }}" class="form-control" name="phone"  id="phone" maxlength="10" minlength="10" required>
                    </div>
                    <div class="form-group form-check">
                        <input class="form-check-input" type="checkbox" name="ga_post" value="Yes" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            Are you a Georgia POST instructor and wanting access to LEMSt?
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>If you are requesting to attend please enter your Okey number.</b></label>
                        <input type="text"  value="{{ old('officer_key_number') }}" class="form-control" name="officer_key_number">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Questions or Suggestions</b></label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="questions"></textarea>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Contact">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
