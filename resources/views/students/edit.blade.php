@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Edit Student</b>
                <div class="card-subtitle mb-2 text-muted"><i>Student information will be updated across LEMSt.</i></div>
            </div>
            <form method="POST" action="{{ route('update_student') }}">
                @csrf
                <input type="hidden" name="student_id" value="{{$student->id}}">
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>First Name</b></label>
                        <input type="text" class="form-control" name="first_name" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->first_name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Last Name</b></label>
                        <input type="text" class="form-control" name="last_name" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->last_name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Suffix</b></label>
                        <select class="form-control" id="suffix" name="suffix">
                            <option value="{{$student->suffix}}" selected="selected">{{$student->suffix}}</option>
                            <option value="Jr.">Jr.</option>
                            <option value="Sr.">Sr.</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Email</b></label>
                        <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->email}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Phone Number</b></label>
                        <input type="text" class="form-control" name="phone"  id="phone" maxlength="10" minlength="10" value="{{$student->phone}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Okey</b></label>
                        <input type="text" class="form-control" name="officer_key_number" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->officer_key_number}}" required>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="/students" class="btn btn-secondary">Cancel and Return</a>
                    <input type="submit" value="Update Student" class="btn btn-primary float-right">
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
