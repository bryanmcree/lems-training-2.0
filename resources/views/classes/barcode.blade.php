@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header">
                <h3><b>{{$class->class_name}}</b></h3>
            </div>
            <div class="card-body text-center">
                @if($class->barcode_on == 0)
                <div class="alert alert-danger" role="alert">
                    The Instructor has not enabled barcode sign in.  Find them, tell them to fix it.
                </div>
                @else
                    <h3 class="justify-content-center">Scan code to sign in!</h3>
                {!! QrCode::size(800)->generate('http://lemstweb.com/signin/barcode/' . $class->id); !!}
                    <h3 class="justify-content-center">If you have a Georgia POST OKey you will need it.</h3>
                    @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
