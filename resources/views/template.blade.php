@extends('layouts.app')

@section('content')
    @if ( Auth::user()->hasRole('Administrator'))
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Dashboard</b></div>

            <div class="card-body">
                You are logged in!
            </div>
        </div>
    </div>
    @else
        @include('unauthorized')
    @endif
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
