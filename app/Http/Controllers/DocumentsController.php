<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Documents;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class DocumentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addFile() {

        $file = Input::file('file_name');
        $extension = $file->getClientOriginalExtension();
        $entry = new Documents();
        $entry->id = Uuid::generate(4);
        $entry->file_mime = $file->getClientMimeType();
        $entry->old_filename = $file->getClientOriginalName();
        $entry->new_filename = Uuid::generate(4) .'.'.$extension;
        $entry->class_id = Input::get('class_id');
        $entry->file_extension = $extension;
        $entry->file_size = $file->getClientSize();
        $entry->file_description = Input::get('file_description');

        Storage::disk('local')->put($entry->new_filename,  File::get($file));

        $entry->save();
        alert()->success('File has been uploaded.', 'File Uploaded');
        return redirect('/classes/detail/'.Input::get('class_id'));
    }

    public function downloadFile($id){

        $entry = Documents::where('id', '=', $id)->firstOrFail();
        $file = Storage::disk('local')->get($entry->new_filename);
        return (new Response($file, 200))
            ->header('Content-Type', $entry->file_mime)
            ->header('content-disposition', 'attachment; filename="'. $entry->old_filename .'"');
    }

    public function deleteFile($id, $class_id)
    {
        $entry = Documents::where('id', '=', $id)->firstOrFail();
        $file = Storage::disk('local')->get($entry->new_filename);
        File::delete($file);
        return redirect('/classes/'.$class_id);
    }

    public function add_document($id){

        $class = Classes::find($id);
        return View('documents.add_document', compact('class'));
    }
}
