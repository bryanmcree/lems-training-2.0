<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Public Routes
Route::get('/', function () {
    return view('welcome');
});

Route::get('/about_us', function () {
    return view('about_us');
});

Route::get('/instructors', function () {
    return view('instructors');
});


//Sends test email
Route::get('test_email', function(){ Mail::raw('This is a test email 1 ...2', function($message) { $message->to('bryanmcree@gmail.com'); }); });
Route::GET('/signin/barcode/{id}', 'PublicController@barcode_sign_in');
Route::get('/contact_us','PublicController@contact_us');
Route::POST('/contact_us/submit','PublicController@contact_us_submit')->name('contact_us_submit');
Route::GET('/classes/public', 'PublicController@public_classes');
Route::GET('/contact/instructor/{id}', 'PublicController@contact_instructor');
Route::POST('/contact/instructor/submit', 'PublicController@contact_instructor_submit')->name('contact_instructor_submit');
Route::POST('/classes/student/roster', 'ClassesController@addStudent_roster')->name('student_roster');
Route::GET('/class/signin/barcode/{id}', 'ClassesController@barcode_sign_in');
Route::POST('/classes/signin/manual/add', 'ClassesController@roster_manual_add')->name('roster_manual_add');
Route::GET('/jquery/students', 'StudentsController@searchStudents');
Route::GET('/class/signin/manual/{id}', 'ClassesController@roster_manual');




//Secure Routes
Auth::routes();

if (Gate::allows('instructors_only', Auth::user())) {
    // The current user can update the post...
}
Route::GET('/users/sendlink','UsersController@send_link')->middleware('auth');
Route::POST('/users/sendlink/send', 'UsersController@send_email')->name('send_link_email')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/classes', 'ClassesController@index')->name('classes')->middleware('auth');
Route::get('/classes/detail/{id}', 'ClassesController@detail')->name('classes_detail')->middleware('auth');
Route::get('classes/add_class', 'ClassesController@add_class')->name('add_class')->middleware('auth');
Route::GET('/classes/signature/{id}', 'ClassesController@studentsSignature')->middleware('auth');
Route::POST('/classes/signature/', 'ClassesController@storeSignature')->name('store_signature')->middleware('auth');
Route::GET('/jquery/classes', 'ClassesController@searchClasses')->middleware('auth');
Route::POST('/classes/add/new/', 'ClassesController@new_class')->name('new_class')->middleware('auth');

Route::POST('/classes/student/add', 'ClassesController@addStudent')->name('student_fast_add')->middleware('auth');
Route::GET('/classes/edit/student/{id}', 'ClassesController@edit_student')->middleware('auth');
Route::POST('/classes/edit/student/', 'ClassesController@update_student')->name('update_student')->middleware('auth');
Route::GET('/classes/add/student/{id}', 'ClassesController@manual_student')->middleware('auth');
Route::POST('/classes/add/update/', 'ClassesController@manual_student_add')->name('manual_student_add')->middleware('auth');
Route::GET('/classes/post/{id}', 'ClassesController@postExport')->middleware('auth');
Route::get('/classes/remove/c={class_id}&s={students_id}', 'ClassesController@removeStudent')->middleware('auth');
Route::get('/scores/detail/{id}', 'ScoresController@scoresDetail')->middleware('auth');
Route::GET('/classes/add/scores/{id}', 'ScoresController@class_input')->middleware('auth');
Route::POST('/classes/add/score/new/', 'ScoresController@class_input_add')->name('new_score')->middleware('auth');
Route::GET('/chart/volumes', 'HomeController@volumes')->middleware('auth');
Route::GET('/chart/volumes/year', 'HomeController@volumes_year')->middleware('auth');
Route::GET('/chart/volumes/classes', 'HomeController@classes_type')->middleware('auth');
Route::GET('/users', 'UsersController@index')->middleware('auth');
Route::GET('/users/{id}', 'UsersController@edit_user')->middleware('auth');
Route::POST('/users/update', 'UsersController@update_user')->name('update_user');
Route::GET('/students', 'StudentsController@index')->middleware('auth');
//Returns QR Barcode
Route::get('/classes/barcode/{id}', 'ClassesController@barcode_signin')->middleware('auth');
Route::GET('/classes/signin/{id}', 'ClassesController@sign_in')->middleware('auth');
Route::GET('/classes/signin/signature/{id}', 'ClassesController@sign_in_studentsSignature')->middleware('auth');
Route::GET('/students/edit/{id}', 'StudentsController@edit_student')->middleware('auth');
Route::GET('/scores/edit/{id}', 'ScoresController@edit_score')->middleware('auth');
Route::POST('/scores/edit/update', 'ScoresController@update_score')->name('update_score')->middleware('auth');
Route::POST('/classes/edit/update', 'ClassesController@update_class')->name('update_class')->middleware('auth');

Route::GET('/classes/admin/{id}', 'ClassesController@index_admin')->middleware('auth');
Route::POST('/classes/documents/add', 'DocumentsController@addFile')->name('addfile')->middleware('auth');
Route::GET('/classes/documents/download/{id}', 'DocumentsController@downloadFile')->middleware('auth');
Route::GET('/classes/documents/add/{id}', 'DocumentsController@add_document')->middleware('auth');
Route::GET('/students/add/', 'StudentsController@create_student')->middleware('auth');
Route::POST('/students/add/new', 'StudentsController@add_student')->name('add_student')->middleware('auth');
Route::get('/class/pdf/{id}','ClassesController@print_pdf')->middleware('auth');
Route::GET('/students/detail/{id}', 'StudentsController@detail')->middleware('auth');
Route::GET('/history', 'HistoryController@index')->middleware('auth');
Route::GET('/agencies', 'AgenciesController@index')->middleware('auth');
Route::GET('/agencies/add', 'AgenciesController@add')->middleware('auth');
Route::POST('/agencies/add/store', 'AgenciesController@store_agency')->name('store_agency')->middleware('auth');
Route::GET('/roles', 'RoleController@index')->middleware('auth');
Route::GET('/roles/add', 'RoleController@add')->middleware('auth');
Route::POST('/roles/store', 'RoleController@store_role')->name('store_role')->middleware('auth');
Route::GET('/users/security/{id}', 'UsersController@user_security')->middleware('auth');
Route::POST('/users/security/add', 'UsersController@add_roles')->name('add_roles')->middleware('auth');
Route::POST('/users/security/remove', 'UsersController@remove_roles')->name('remove_roles')->middleware('auth');
Route::GET('/users/login_as/{id}','UsersController@login_as')->middleware('auth');
Route::GET('/class/delete/{id}','ClassesController@delete_class')->middleware('auth');
Route::GET('/student/delete/{id}','StudentsController@delete_student')->middleware('auth');
Route::GET('/score/delete/{id}','ScoresController@delete_score')->middleware('auth');

//Home page
Route::GET('/home/private/{id}','HomeController@togglePrivate')->middleware('auth');
Route::GET('/home/complete/{id}','HomeController@markComplete')->middleware('auth');


