@extends('layouts.app')

@section('content')
    <div class="col-lg-12">
        <div class="card mt-2">
            <div class="card-header"><b>{{$class->course_id}}-{{$class->class_name}} ({{$class->total_hours}} hours)</b>
                <div class="card-subtitle mb-2 text-muted"><i>{{$class->location}}</i></div>
                @if(\Carbon\Carbon::now() > $class->start_date AND $class->completed == 'No')
                    @if(30-(\Carbon\Carbon::now()->diffInDays($class->start_date)) < 1)
                        <div class="alert alert-danger">You have {{30-(\Carbon\Carbon::now()->diffInDays($class->start_date))}} days to submit this class to POST. This class is past due.</div>
                    @else
                        <div class="alert alert-warning">You have {{30-(\Carbon\Carbon::now()->diffInDays($class->start_date))}} days to submit this class to POST.</div>
                    @endif
                @endif
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-students-tab" data-toggle="tab" href="#nav-students" role="tab" aria-controls="nav-students" aria-selected="true"><i class="fad fa-users"></i> Students</a>
                            <a class="nav-item nav-link" id="nav-scores-tab" data-toggle="tab" href="#nav-scores" role="tab" aria-controls="nav-scores" aria-selected="false"><i class="fas fa-bullseye-arrow"></i> Scores</a>
                            <a class="nav-item nav-link" id="nav-files-tab" data-toggle="tab" href="#nav-files" role="tab" aria-controls="nav-files" aria-selected="false"><i class="fal fa-file-spreadsheet"></i> Documents</a>
                            <a class="nav-item nav-link" id="nav-edit-tab" data-toggle="tab" href="#nav-edit" role="tab" aria-controls="nav-edit" aria-selected="false"><i class="fas fa-edit"></i> Edit</a>
                            <a class="nav-item nav-link" id="nav-delete-tab" data-toggle="tab" href="#nav-delete" role="tab" aria-controls="nav-delete" aria-selected="false"><i class="fad fa-trash-alt"></i> Delete Class</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-students" role="tabpanel" aria-labelledby="nav-students-tab">
                            <div class="card mt-2">
                                <div class="card-body">
                                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                                        <a class="navbar-brand text-white">Students ({{$class->studentsClass->count()}})</a>
                                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_students" aria-controls="navbarSupportedContent_students" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbarSupportedContent_students">
                                            <ul class="navbar-nav mr-auto">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="/classes/post/{{$class->id}}">Export Roster<span class="sr-only">(current)</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="/class/pdf/{{$class->id}}" target="_blank">Print Class</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="/classes/add/student/{{$class->id}}">Manual Add</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="/classes/add/scores/{{$class->id}}">Input Scores</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link @if($class->barcode_on == 0) disabled @endif" href="/classes/barcode/{{$class->id}}" target="_blank">Sign in Barcode</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="/classes/signin/{{$class->id}}" target="_blank">Sign in</a>
                                                </li>
                                            </ul>
                                            <ul class="navbar-nav ">
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="collapse" href="#help"><i class="fad fa-walkie-talkie" title="User needs assistance..."></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <form class="form-inline my-2 my-lg-0" method="POST" action="{{ route('student_fast_add') }}">
                                            @csrf
                                            <input type="hidden" name="classes_id" value="{{$class->id}}">
                                            <input type="hidden" name="students_id" id="students_id">
                                            <div class="input-group">
                                                <select class="searchstudents form-control-lg" style="width: 300px;"></select>
                                                <div class="input-group-append">
                                                    <input type="submit" class="btn btn-primary btn-sm">
                                                </div>
                                            </div>
                                        </form>
                                    </nav>
                                    <div class="collapse" id="help">
                                        <div class="card card-body mt-2 mb-2">
                                            <p>This is the heart of lems.  From here you can add students to your class.  <b>Export Roster</b> lets you download a CSV file of all the officers signed up for this class
                                            and can be imported at GA POST.  <b>Print Class</b> creates a print friendly view that lets you print to paper or PDF your roster with signatures and scores to be uploaded
                                            to POST or kept in a physical training file.  <b>Manual Add</b> allows you to quickly add students to both lems and the class at the same time.  <b>Input Scores</b> allows you
                                            to enter scores for each student.  Scores can be either practical, written or firearms.  <b>Sign in Barcode</b> provides a QR barcode that allows students to scan with their
                                            phone and sign in themselves.  This option needs to be turned on in the 'Edit' class tab.  <b>Sign in</b> opens a screen that allows students to sign in, perfect for a tablet
                                            to be passed around a small class.  <b>Signatures</b> can be added and changed by clicking on the signature.  To <b>Remove a student</b> use the 'Options' dropdown next to the
                                            students name and select 'Remove Student'.  <b>Documents</b> such as powerpoints, lesson plans, and images can be added to the class.  This is great for record keeping as
                                            training material is constantly updated.  It is important to keep records of the resources used for this particular class.</p>
                                        </div>
                                    </div>
                                    <table class="table table-hover table-striped" id="students" >
                                        <thead>
                                            <tr>
                                                <td class="d-none d-sm-table-cell"><b>Okey</b></td>
                                                <td><b>Name</b></td>
                                                <td class="d-none d-lg-table-cell"><b>Phone</b></td>
                                                <td class="d-none d-lg-table-cell"><b>Email</b></td>
                                                <td class="d-none d-sm-table-cell" align="center"><b>Signature</b></td>
                                                <td class="d-none d-xl-table-cell" align="center"><b>Signed On</b></td>
                                                <td><b></b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($class->studentsClass as $students)
                                            <tr>
                                                <td class="d-none d-sm-table-cell">{{$students->officer_key_number}}</td>
                                                <td>{{$students->last_name}}, {{$students->first_name}}</td>
                                                <td class="d-none d-lg-table-cell">{{PhoneNumbers::formatNumber($students->phone)}}</td>
                                                <td class="d-none d-lg-table-cell"><a href="mailto:{{$students->email}}"> {{$students->email}}</a></td>
                                                <td class="d-none d-sm-table-cell" align="center">@if($students->pivot->signature == NULL)<a href="/classes/signature/{{$class->id}}?S={{$students->id}}" class="btn btn-block btn-warning"> <i class="fa fa-exclamation-triangle " style="color:red;" aria-hidden="true"></i> Sign</a>
                                                    @else <a href="/classes/signature/{{$class->id}}?S={{$students->id}}"><img  src='data:image/svg+xml; {{$students->pivot->signature}}' style="height: 30px;"></a> @endif</td>
                                                <td class="d-none d-xl-table-cell" align="center">@if(is_null($students->pivot->signature_date)) No Date @else {{\Carbon\Carbon::parse($students->pivot->signature_date)->format('m-d-Y H:i')}} @endif</td>
                                                <td nowrap="" align="right">
                                                    <ul class="nav nav-pills float-right">
                                                        <li class="nav-item dropdown">
                                                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Options</a>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="/scores/detail/{{$class->id}}?S={{$students->id}}"><i class="fas fa-bullseye-pointer" style="color:black"></i> View Score</a></a>
                                                                <a class="dropdown-item" href="#"><i class="fas fa-bullseye-pointer" style="color:green"></i> Add Score</a>
                                                                <a class="dropdown-item" href="/classes/edit/student/{{$class->id}}?S={{$students->id}}"><i class="fas fa-user-edit" style="color:blue"></i> Edit Student</a>
                                                                <div class="dropdown-divider"></div>
                                                                <a class="dropdown-item" href="#" id="delete_student" data-class_id = "{{$class->id}}" data-students_id = "{{$students->id}}"><i class="fas fa-user-slash" style="color:red"></i> Remove Student</a>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-scores" role="tabpanel" aria-labelledby="nav-scores-tab">
                            <div class="card mt-2">
                                <div class="card-header">
                                    <b>Class Scores</b> <a href="/classes/add/scores/{{$class->id}}" class="btn btn-info btn-sm float-right">Input Scores</a>
                                </div>
                                <div class="card-body">
                                    @if($class->studentScores->where('class_id', $class->id)->isEmpty())
                                        <div class="alert alert-info">No scores</div>
                                    @else
                                    <table class="documents table table-condensed table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <td></td>
                                            <td><b>Student Name</b>
                                            <td class="d-none d-lg-table-cell"><b>O Avg</b></td>
                                            <td><b>C Avg</b></td>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach ($class->studentsClass as $students)
                                            <tr>
                                                <td><a href="/scores/detail/{{$class->id}}?S={{$students->id}}"><i class="fad fa-info-circle"></i></a></td>
                                                <td><b>{{$students->last_name}}, {{$students->first_name}}</b></td>
                                                <td class="d-none d-lg-table-cell">{{number_format($students->scores->where('students_id', $students->id)->avg('score_percent'),0)}}%</td>
                                                <td>{{number_format($students->scores->where('class_id', $class->id)->avg('score_percent'),0)}}%</td>
                                                @foreach ($students->scores->where('class_id', $class->id) as $scores)
                                                    <td nowrap=""><a href="/scores/edit/{{$scores->id}}?C={{$class->id}}" class="btn btn-block @if($scores->fail =='Fail') btn-danger @else btn-success @endif btn-sm">
                                                        @if($scores->type == 'Firearms')<i class="far fa-bullseye"></i>
                                                        @elseif($scores->type == 'Written Test')W
                                                        @elseif($scores->type == 'Practical')P
                                                        @endif{{$scores->score_percent}}%</a>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-files" role="tabpanel" aria-labelledby="nav-files-tab">
                            <div class="card mt-2">
                                <div class="card-body">
                                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                                        <a class="navbar-brand text-white">Documents ({{$class->documentsClass->count()}})</a>
                                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_students" aria-controls="navbarSupportedContent_students" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbarSupportedContent_students">
                                            <ul class="navbar-nav mr-auto">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="/classes/documents/add/{{$class->id}}">Add Document<span class="sr-only">(current)</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <<form class="form-inline my-2 my-lg-0">
                                            <input class="form-control mr-sm-1" type="search" placeholder="Search" aria-label="Search" id="documentsSearch">
                                        </form>
                                    </nav>
                                    @if($class->documentsClass->isEmpty())
                                        <div class="alert alert-info mt-2">No documents uploaded.</div>
                                    @else
                                        <table class="table table-hover table-striped" id="documents" width="100%">
                                            <thead>
                                            <tr>
                                                <td><B>Date Added</B></td>
                                                <td><B>File Type</B></td>
                                                <td><B>File Size</B></td>
                                                <td><B>File Name</B></td>
                                                <td><B>Description</B></td>
                                                <td></td>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach ($class->documentsClass as $documents)
                                                <tr>
                                                    <td>{{$documents->created_at->format('m-d-Y H:i')}}</td>
                                                    <td>{{$documents->file_extension}}</td>
                                                    <td>{{number_format($documents->file_size)}}</td>
                                                    <td>{{$documents->old_filename}}</td>
                                                    <td>{{$documents->file_description}}</td>
                                                    <td>
                                                        <div class="dropdown" align="right">
                                                            <button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Options<span class="caret"></span></button>
                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                                                <li role="presentation"><a role="menuitem" href="/classes/documents/download/{{$documents->id}}"><i class="fa fa-download"></i> Download</a></li>
                                                                <li role="presentation"><a role="menuitem" href="/classes/documents/download/{{$documents->id}}"><i class="fa fa-pencil-square-o"></i> Edit</a></li>
                                                                <li role="presentation" class="divider"></li>
                                                                <li role="presentation"><a role="menuitem" href="/classes/signature/{{$documents->id}}"><i class="fa fa-trash" style="color:red;"></i> Remove Document</a></li>
                                                            </ul>
                                                        </div>
                                                    </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-edit" role="tabpanel" aria-labelledby="nav-edit-tab">
                            <div class="card mt-2">
                                <div class="card-header">
                                    <b>Update Class</b>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action="{{ route('update_class') }}">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$class->id}}">
                                        <input type="submit" class="btn btn-primary float-right" value="Update Class" >
                                        <input type="reset" value="Clear Changes" class="btn btn-secondary">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><b>Course Number</b></label>
                                            <input type="text" name="course_id" value="{{$class->course_id}}" class="form-control" id="course_id" aria-describedby="coursenumber" placeholder="Course Number" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><b>Course Name</b></label>
                                            <input type="text" name="class_name" value="{{$class->class_name}}" class="form-control" id="class_name" aria-describedby="coursenumber" placeholder="Course Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><b>Course Location</b></label>
                                            <input type="text" name="location" value="{{$class->location}}" class="form-control" id="course_name" aria-describedby="coursenumber" placeholder="Where is this class located.">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><b>Start Date</b></label>
                                            <input type="date" name="start_date" value="{{$class->start_date->format('Y-m-d')}}" class="form-control" id="course_name" aria-describedby="coursenumber" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><b>Start Time</b></label>
                                            <input type="time" name='start_time' value="{{$class->start_time}}" class="form-control" id="course_name" aria-describedby="coursenumber" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><b>Total Hours</b></label>
                                            <input type="number" name="total_hours" value="{{$class->total_hours}}" class="form-control" id="course_name" placeholder="Total Hours" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><b>Class Notes</b></label>
                                            <small id="emailHelp" class="form-text text-muted">Information entered into this box will be visable to the student.</small>
                                            <textarea class="form-control" name="studentsnotes" rows="3" id="student_notes">{{$class->studentsnotes}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1"><b>Visible by public?</b></label>
                                            <select class="form-control" name="class_public">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                                <option value="{{$class->class_public}}" selected>@if($class->class_public == 1) Yes @else No @endif</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1"><b>Barcode sign-in on?</b></label>
                                            <select class="form-control" name="barcode_on">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                                <option value="{{$class->barcode_on}}" selected>@if($class->barcode_on == 1) Yes @else No @endif</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1"><b>Class Completed?</b></label>
                                            <select class="form-control" name="completed">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                                <option value="{{$class->completed}}" selected>{{$class->completed}}</option>
                                            </select>
                                        </div>
                                        <input type="submit" class="btn btn-primary float-right" value="Update Class" >
                                        <input type="reset" value="Clear Changes" class="btn btn-secondary">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-delete" role="tabpanel" aria-labelledby="nav-delete-tab">
                            <div class="card mt-2">
                                <div class="card-body">
                                    <p>Deleting class will remove class from all list and views.  Don't worry.  All records, students scores and even the class can be recovered.</p>
                                    <a href="/class/delete/{{$class->id}}" class="btn btn-danger">Delete {{$class->class_name}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        oTable = $('#students').DataTable({
            bFilter: true,
            ordering: false,
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            dom: 'ltipr'         // This shows just the table
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })


        oTable = $('#documents').DataTable({
            bFilter: true,
            ordering: false,
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            dom: 'ltipr'         // This shows just the table
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#documentsSearch').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })


        $(function () {
            $('.searchstudents').select2({
                placeholder: "Student Search",
                minimumInputLength: 2,
                width: 'resolve', // need to override the changed default
                ajax: {
                    url: '/jquery/students',
                    dataType: 'json',
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

            $('.searchstudents').on('select2:select', function (e) {
                var students_id = e.params.data.id;
                var class_name = e.params.data.text;
                //console.log(data);
                //alert(students_id);
                $("#students_id").val(students_id);
                $("#class_name").val(class_name);
            });
        })

        $('#btnSave').click(function(){
            var sigData = $('#signature').jSignature('getData','base30');
            $('#offsignature').val(sigData);
            alert($('#signature').jSignature('getData','base30'));
        });


        //Remove student from class.

        $(document).ready(function(){


            $(document).on('click', '#delete_student', function(e){

                var class_id = $(this).data('class_id');
                var students_id = $(this).data('students_id');
                SwalDelete(class_id,students_id);
                e.preventDefault();
            });

        });

        function SwalDelete(class_id,students_id){
            swal({
                title: "Remove student from class?",
                text: "This will remove student from this class.",
                icon: "warning",
                buttons: true,
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: false,
                    className: "",
                    closeModal: true,
                },

            }).then(function(yes) {
                if (yes) {
                    $.ajax({
                        url: "/classes/remove/c="+class_id+"&s="+students_id ,
                        method: "GET",
                        success: function(data){
                            swal({title: "Removed", text: "Student was removed!", type:
                                "success"}).then(function(){
                                    location.reload();
                                }
                            );

                        }
                    });
                }
                else {
                    return false;
                }
            });
        };

    </script>
@endsection
