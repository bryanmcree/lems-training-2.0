@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Add Class</b></div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Search Classes</b><i> (Optional)</i></label>
                        <select class="js-data-example-ajax form-control" name="query"></select>
                    </div>

                    <hr>
                    <form method="POST" action="{{ route('new_class') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Course Number</b></label>
                            <input type="text" name="course_id" class="form-control" id="course_id" aria-describedby="coursenumber" placeholder="Course Number" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Course Name</b></label>
                            <input type="text" name="class_name" class="form-control" id="class_name" aria-describedby="coursenumber" placeholder="Course Name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Course Location</b></label>
                            <input type="text" name="location" class="form-control" id="course_name" aria-describedby="coursenumber" placeholder="Where is this class located.">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Start Date</b></label>
                            <input type="date" name="start_date" class="form-control" id="course_name" aria-describedby="coursenumber" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Start Time</b></label>
                            <input type="time" name='start_time' class="form-control" id="course_name" aria-describedby="coursenumber" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Total Hours</b></label>
                            <input type="number" name="total_hours" class="form-control" id="course_name" placeholder="Total Hours" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><b>Class Notes</b></label>
                            <small id="emailHelp" class="form-text text-muted">Information entereed into this box will be visable to the student.</small>
                            <textarea class="form-control" name="studentsnotes" rows="3" id="student_notes"></textarea>
                        </div>
                        <input type="submit" class="btn btn-primary float-right" value="Add Class">
                        <input type="reset" class="btn btn-secondary">
                    </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

        $(function () {
            $('.js-data-example-ajax').select2({
                placeholder: "Search for a class",
                minimumInputLength: 2,
                ajax: {
                    url: '/jquery/classes',
                    dataType: 'json',
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

            $('.js-data-example-ajax').on('select2:select', function (e) {
                var course_id = e.params.data.id;
                var class_name = e.params.data.text;
                //console.log(data);
                $("#course_id").val(course_id);
                $("#class_name").val(class_name);
            });
        })


    </script>
@endsection
