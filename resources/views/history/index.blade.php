@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>History</b></div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <td><b>UserName</b></td>
                            <td><b>Function</b></td>
                            <td><b>IP</b></td>
                            <td><b>Notes</b></td>
                            <td><b>Created At</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($history_log as $log)
                        <tr>
                            <td>{{$log->username}}</td>
                            <td>{{$log->function}}</td>
                            <td>{{$log->ip_address}}</td>
                            <td>{{$log->notes}}</td>
                            <td>{{$log->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $history_log->links() }}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
