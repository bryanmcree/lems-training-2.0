@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <form method="POST" action="{{ route('store_signature') }}">
            @csrf
            <input type="hidden" name="signature" id="offsignature">
            <input type="hidden" name="students_id" value="{{$student->id}}">
            <input type="hidden" name="classes_id" value="{{$class->id}}">
        <div class="card mt-2">
            <div class="card-header"> Signature of <b>{{$student->last_name}}, {{$student->first_name}}</b> who attest they attended <i>{{$class->class_name}}</i> and have met all
                the minimum requirements for credit.</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                    <div id="signature"></div>
            </div>
            <div class="card-footer">
                <i class="far fa-arrow-square-up"></i> Sign Here
            </div>
            <div class="card-body">
                <input type="submit" value="Add Signature and Return to Class" id="btnSave" class="btn btn-primary btn-block">
            </div>
            <div class="card-body">
                <button type="button" class="btn btn-secondary" onclick="$('#signature').jSignature('clear')">Clear</button>
                <a href="/classes/detail/{{$class->id}}" class="btn btn-warning float-right">Cancel and Return to Class</a>
            </div>
        </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        $(document).ready(function() {
            $("#signature").jSignature({
                color: "blue",
                lineWidth: 1,
                showLine: true
            })
        });

        $('#btnSave').click(function(){
            var sigData = $('#signature').jSignature('getData','svgbase64');
            $('#offsignature').val(sigData);

        });

    </script>
@endsection
