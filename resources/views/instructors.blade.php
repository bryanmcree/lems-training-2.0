@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Instructors</b></div>

            <div class="card-body">
                <p>The primary focus of LEMSt is records management for Georgia POST Certified Instructors and for the moment is working on a referral only basis.  In time
                    we would like to include students.  Since 2017 LEMSt was used as a playground by the designer to develop easy electronic methods of collecting, organizing,
                    quantifying and displaying data.  Every Law Enforcement Officer knows how important it is to document and keep records training records are no exception.  The best part
                about LEMSt is that there is no cost to instructors or students.</p>



                <div class="row">
                    <div class="col-2">
                        <div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home"><i class="fad fa-chalkboard-teacher"></i> <b>Classes</b></a>
                            <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile"><i class="fad fa-users-class"></i> <b>Students</b></a>
                            <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages"><i class="fad fa-graduation-cap"></i> <b>Scores</b></a>
                            <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings"><i class="fad fa-battery-full"></i> <b>Efficiency</b></a>
                            <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-pricing" role="tab" aria-controls="settings"><i class="fad fa-sack-dollar"></i> <b>Pricing</b></a>
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                                <p>Classes can be added and shared publicly to get the word out about upcoming training.  Class names and POST course numbers are saved making adding new
                                classes easy and fast.  Documents can easily be added to keep training material such as lesson plans and powerpoints together.  Students are then added to the
                                class.  Classes can then be printed to PDF and uploaded to POST including signatures and scores.</p>
                                <img class="img-responsive mx-auto d-block" img src="/img/class_image_web.PNG" alt="Class Detail">
                            </div>
                            <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                                <p>Students can be added to LEMSt many ways.  They can be manually added to a class, selected from a search list, or even added by the student themselves using a QR barcode.
                                Once the student is added to LEMSt their information will be shared with other instructors allowing student to easily attend other classes and keeping all their information
                                in one place.  Each student can sign a roster using their finger on a portable device such as a phone or Ipad.</p>
                                <img class="img-responsive mx-auto d-block" img src="/img/scores_image_web.PNG" alt="Student Scores">
                            </div>
                            <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                                <p>LEMSt will track many different types of scores such as firearms scores, written test scores, and practical pass or fail scores.  Each score is attached to a student in a
                                class and scores are unique to each class.  Over a period of time student scores can be tracked, charted and organized to identify training issues and track progress.  With an
                                internet connection instructors can enter scores directly on the range reducing errors and ensuring accuracy.</p>
                                <img class="img-responsive mx-auto d-block" img src="/img/charts_image_web.PNG" alt="Dashboard">
                            </div>
                            <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                                <p>Ease of use, multiple platform comparability makes LEMSt the best RMS for law enforcement training.</p>
                            </div>
                            <div class="tab-pane fade" id="list-pricing" role="tabpanel" aria-labelledby="list-settings-list">
                                <p><b>LEMSt will always be free to individual law enforcement officers.</b>  There is a plan to offer an agency option that would allow multiple officers / instructors within an agency to access
                                data. While this feature is still being developed the anticipated price will be around $250 per year.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
