<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Students extends Model
{
    use SoftDeletes;
    protected $table = 'students';
    public $incrementing = false;
    protected $guarded = [];

    public function studentsClass(){
        return $this->belongsToMany('\App\Classes','classes_students')->withPivot('signature','signature_date')->orderby('start_date','desc');
    }

    public function scores(){
        return $this->hasMany('\App\Scores','students_id','id')->orderBy('created_at');
    }

    public function scoresDes(){
        return $this->hasMany('\App\Scores','students_id','id')->orderBy('created_at','desc');
    }

    public function classesCount(){
        return $this->hasMany('\App\ClassStudentPiviot', 'students_id');
    }

    public function enteredBy(){
        return $this->hasOne('\App\User', 'account_number', 'account_number');
    }

    public function sharedStudent(){
        return $this->hasOne('\App\AccountOptions', 'account_number', 'account_number');
    }


}
