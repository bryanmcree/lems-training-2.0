@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <form class=""  method="POST" action="{{ route('student_roster') }}">
                @csrf
                <input type="hidden" name="class_id" value="{{$class->id}}">
                <input type="hidden" name="student_id" id="student_id">
                <input type="hidden" name="signature" id="offsignature">
                <input type="hidden" name="barcode" value="{{$barcode}}">
            <div class="card-header"><b>{{$class->class_name}}</b></div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><b>Search for name or Okey number, select then click add to class.</b></label>
                                <select class="searchstudents form-control" required></select>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <a href="/class/signin/manual/{{$class->id}}?barcode={{$barcode}}" class="btn btn-warning btn-block">I was not listed in the search to the left, or I have never done this before.  I need to enter my name and Okey manually.</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <b>Sign Below</b>
            </div>
            <div class="card-body">
                <div id="signature"></div>
                <button type="button" class="btn btn-secondary" onclick="$('#signature').jSignature('clear')">Clear Signature</button>
                <input type="submit" id="btnSave" value="Enroll in Class" class="btn btn-primary float-right">
            </div>
            </form>
            @if($barcode != 1 )
            <div class="card-body">
                <table class="table table-hover table-striped" id="students" >
                    <thead>
                    <tr>
                        <td class="d-none d-sm-table-cell"><b>Okey</b></td>
                        <td><b>Name</b></td>
                        <td class="d-none d-lg-table-cell"><b>Phone</b></td>
                        <td class="d-none d-lg-table-cell"><b>Email</b></td>
                        <td class="d-none d-sm-table-cell" align="center"><b>Signature</b></td>
                        <td class="d-none d-xl-table-cell" align="center"><b>Signed On</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($class->studentsClass as $students)
                        <tr>
                            <td class="d-none d-sm-table-cell">{{$students->officer_key_number}}</td>
                            <td>{{$students->last_name}}, {{$students->first_name}}</td>
                            <td class="d-none d-lg-table-cell">{{PhoneNumbers::formatNumber($students->phone)}}</td>
                            <td class="d-none d-lg-table-cell"><a href="mailto:{{$students->email}}"> {{$students->email}}</a></td>
                            <td class="d-none d-sm-table-cell" align="center">@if($students->pivot->signature == NULL)<a href="/classes/signature/{{$class->id}}?S={{$students->id}}" class="btn btn-block btn-warning"> <i class="fa fa-exclamation-triangle " style="color:red;" aria-hidden="true"></i> Sign</a>
                                @else <a href="/classes/signin/signature/{{$class->id}}?S={{$students->id}}"><img  src='data:image/svg+xml; {{$students->pivot->signature}}' style="height: 30px;"></a> @endif</td>
                            <td class="d-none d-xl-table-cell" align="center">@if(is_null($students->pivot->signature_date)) No Date @else {{\Carbon\Carbon::parse($students->pivot->signature_date)->format('m-d-Y H:i')}} @endif</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        $(function () {
            $('.searchstudents').select2({
                placeholder: "Student Search",
                minimumInputLength: 2,
                allowClear: true,
                ajax: {
                    url: '/jquery/students',
                    dataType: 'json',
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

            $('.searchstudents').on('select2:select', function (e) {
                var student_id = e.params.data.id;
                //alert(student_id);
                $("#student_id").val(student_id);
            });
        })

        $(document).ready(function() {
            $("#signature").jSignature({
                color: "blue",
                lineWidth: 1,
                showLine: true
            })
        });

        $('#btnSave').click(function(){
            var sigData = $('#signature').jSignature('getData','svgbase64');
            $('#offsignature').val(sigData);

        });
    </script>
@endsection
