<?php

namespace App\Http\Controllers;

use App\Classes;
use App\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $incompleted = Classes::where('account_number','=', Auth::user()->account_number)
            ->where('start_date','<=', Carbon::now())
            ->where('completed','=','No')
            ->orderBy('start_date')
            ->get();

        $upcoming = Classes::where('account_number','=', Auth::user()->account_number)
            ->where('start_date','>=', Carbon::now())
            ->orderby('start_date')
            ->get();

        $history = new History();
        $history->function = 'Viewed Dashboard';
        $history->username = \Auth::user()->email;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->save();

        return view('home', compact('incompleted','upcoming'));
    }

    public function volumes(){

        $chart = Classes::select(
            \DB::raw('count(*) as total'),
            \DB::raw("MONTHNAME(start_date) as month"),
            \DB::raw("MONTH(start_date) as month_number"))
            ->where('account_number','=', Auth::user()->account_number)
            ->groupby('month','month_number')
            ->orderby('month_number')
            ->get();
        return $chart;

    }

    public function volumes_year(){

        $chart = Classes::select(
            \DB::raw('count(*) as total'),
            \DB::raw("year(start_date) as class_year"))
            ->where('account_number','=', Auth::user()->account_number)
            ->groupby('class_year')
            ->orderby('class_year')
            ->get();
        return $chart;

    }

    public function classes_type(){

        $chart = Classes::select(
            \DB::raw('count(*) as total'),
            \DB::raw("class_name as class"))
            ->where('account_number','=', Auth::user()->account_number)
            ->groupby('class')
            ->get();
        return $chart;

    }

    public function contact_us(){

        return view('contact_us');
    }

    public function togglePrivate($id)
    {
        $classStatus = Classes::findorfail($id);

        if($classStatus->class_public === 0)
        {
            $classStatus->update(['class_public' => 1]);
        }elseif($classStatus->class_public === 1){
            $classStatus->update(['class_public' => 0]);
        }
        alert()->success('Class has been updated.', 'Class Updated');
        return redirect('/home');
    }

    public function markComplete($id)
    {
        $classStatus = Classes::findorfail($id);

        $classStatus->update(['completed' => 'Yes']);
        alert()->success('Class has been updated.', 'Class Updated');
        return redirect('/home');
    }
}
