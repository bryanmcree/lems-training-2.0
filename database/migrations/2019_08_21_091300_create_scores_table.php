<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('class_id'); // class id
            $table->string('students_id'); //id number from students
            $table->string('account_number');//what user added this
            $table->integer('raw_score'); // raw score 240 out of 300
            $table->integer('base_score'); //max pos for raw score (300)
            $table->string('fail'); //Flag, this student failed
            $table->string('type'); //Firearms, or a test?
            $table->string('course'); //Firearms, or a test?
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
