<?php

namespace App\Exports;

use App\Students;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class StudentRoster implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $StartDate,$EndDate;
    public function __construct($id)
    {
        $this->id = $id;

    }
    public function collection()
    {
        return Students::select('officer_key_number')
            ->Join('classes_students','classes_students.students_id','=','students.id')
            ->where('classes_students.classes_id','=',$this->id)
            ->get();

        //return Students::all();
    }
}
