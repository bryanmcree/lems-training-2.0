@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-body">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">Agencies ({{$agencies->count()}})</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/agencies/add">Add Agency <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="collapse" href="#help"><i class="fad fa-walkie-talkie" title="User needs assistance..."></i></a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-1" type="search" placeholder="Search" aria-label="Search" id="myInputTextField">
                        </form>
                    </div>
                </nav>

                <div class="collapse" id="help">
                    <div class="card card-body mt-2 mb-2">
                        <p>Agencies link user accounts so that many users can share a pool of classes.  Imagine having many officers log into lems and all sharing classes.  Add an agency then add users to that agency.
                            All the users added to an agency will share all data and have the ability to work with each others classes.</p>
                    </div>
                </div>

                @if($agencies->isEmpty())
                    <div class="alert alert-warning">No Agencies Entered</div>
                @else
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <td><b>Agency Name</b></td>
                            <td><b>Admin</b></td>
                            <td><b>Admin Email</b></td>
                            <td><b>City/State</b></td>
                            <td><b>Created At</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($agencies as $agency)
                        <tr>
                            <td>{{$agency->agency_name}}</td>
                            <td>{{$agency->AgencyAdmin->last_name}}, {{$agency->AgencyAdmin->first_name}}</td>
                            <td>{{$agency->AgencyAdmin->email}}</td>
                            <td>{{$agency->city}}, {{$agency->state}}</td>
                            <td>{{$agency->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
