@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">

            <div class="card-body">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">Students ({{$students->count()}})</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/students/add">Add Student <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="collapse" href="#help"><i class="fad fa-walkie-talkie" title="User needs assistance..."></i></a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-1" type="search" placeholder="Search" aria-label="Search" id="myInputTextField">
                        </form>
                    </div>
                </nav>
                <div class="collapse" id="help">
                    <div class="card card-body mt-2 mb-2">
                        <p>Students are added a number of ways either from this screen, the manual feature on the classes page, or barcode sign-up.  Each student has an Okey to allow the intergration with Georgia
                        POST.  Other information such as phone and email are both optional.  Students are encouraged to provide an email and phone number to ensure accuracy of the training record.  Emails should
                        be personal and not departmental as officers can change departments.  Student information is shared across lems with other instructors.</p>
                    </div>
                </div>
                <table class="table table-striped table-hover" id="students">
                    <thead>
                        <tr>
                            <td><b>Okey</b></td>
                            <td><b>Name</b></td>
                            <td><b>Phone</b></td>
                            <td><b>Email</b></td>
                            <td><b>Created At</b></td>
                            <td><b>Updated At</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($students as $student)
                        <tr>
                            <td>{{$student->officer_key_number}} </td>
                            <td>{{$student->last_name}}, {{$student->first_name}} </td>
                            <td>{{PhoneNumbers::formatNumber($student->phone)}}</td>
                            <td>{{$student->email}}</td>
                            <td>{{$student->created_at->format('m-d-Y H:i')}}</td>
                            <td>@if(!is_null($student->updated_at)){{$student->updated_at->format('m-d-Y H:i')}} @endif</td>
                            <td nowrap="" align="right">
                                <a href="students/detail/{{$student->id}}" class="btn btn-primary btn-sm" ><i class="fad fa-user-circle"></i></a>
                                <a href="/students/edit/{{$student->id}}"><i class="fas fa-user-edit" style="color:blue"></i></a>

                                <a href="/scores/detail/{{$student->id}}"><i class="fad fa-dot-circle"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        oTable = $('#students').DataTable({
            bFilter: true,
            ordering: false,
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            pageLength: 50,
            dom: 'ltipr'         // This shows just the table
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    </script>
@endsection
