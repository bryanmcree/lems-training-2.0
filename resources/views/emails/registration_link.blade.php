
<div style="font-family: sans-serif;">
    <table width="100%" align="left" style="border-spacing: 0;border-collapse: collapse;">
        <tr>
            <td colspan="2" style="padding: 0;" bgcolor="#2e9e7b"><h2 style="font-family: sans-serif; color: white">&nbsp; &nbsp; A personal invite.</h2></td>
        </tr>
        <tr>
            <td><p>Law Enforcement Management Systems or LEMS is inviting Georgia POST Certified Instructors to be part of their free record management systems designed just for training.</p></td>
        </tr>
        <tr>
            <td><p>The individual who sent this link to you is already a confirmed user and believes that you could benefit from this.</p></td>
        </tr>
        <tr>
            <td><p>Feel free to visit <a href="https://lemstweb.com/register">LEMSt</a> and start adding you classes. </p></td>
        </tr>
        <tr>
            <td><p>https://lemstweb.com/register</p></td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#d3d3d3"><p>Copyright &copy; lemstweb.com - {{Carbon\Carbon::now()->format('Y')}}</p></td>
        </tr>
    </table>
</div>