<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Scores;
use App\Students;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ScoresController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function scoresDetail($id){

        If(is_null(Input::get('S'))){
            $student_id = $id;
            $class_id = null;
        }else{
            $student_id = Input::get('S');
            $class_id = $id;
        }

        $student = Students::where('id','=', $student_id)
            ->first();
        ;
        return view('scores.scores_detail', compact('class_id','student'));
    }

    public function class_input($id)
    {
        $class = Classes::where('id','=', $id)
            ->where('account_number','=', Auth::user()->account_number)
            ->first();
        return View('scores.class_input', compact('class'));
    }

    public function class_input_add(Request $request)
    {
        //dd($request);
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('account_number', Auth::user()->account_number);
        $request->offsetSet('score_percent', number_format(($request->raw_score / $request->base_score)*100,0));
        $input = $request->all();
        Scores::create($input);
        alert()->success('Score has been recorded.', 'Score Added');
        return redirect('/classes/add/scores/'. $request->class_id);
    }

    public function edit_score($id){

        $class_id = Input::get('C');
        $score = Scores::find($id);
        return View('scores.edit', compact('score','class_id'));
    }

    public function update_score()
    {
        $score = Scores::find(Input::get('id'));
        $score->score_percent = number_format((Input::get('raw_score')/Input::get('base_score'))*100);
        $score->update(Input::all());
        alert()->success('Score has been updated!', 'Success!');
        return redirect('/classes/detail/'. Input::get('class_id'));
    }

    public function delete_score($id)
    {
        Scores::find($id)->delete();
        alert()->success('Score has been deleted.', 'Score Deleted');
        return redirect('/classes/detail/' . Input::get('C'));
    }
}
