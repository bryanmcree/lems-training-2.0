@extends('layouts.app')

@section('content')
    <div class="col-lg-12">
        <div class="card mt-2">

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a class="navbar-brand" href="#">Classes ({{$classes->count()}})</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="/classes/add_class">Add Class <span class="sr-only">(current)</span></a>
                                </li>
                            </ul>
                            <ul class="navbar-nav ">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="collapse" href="#help"><i class="fad fa-walkie-talkie" title="User needs assistance..."></i></a>
                                </li>
                            </ul>
                        </div>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-1" type="search" placeholder="Search" aria-label="Search" id="myInputTextField">
                        </form>
                    </nav>
                    <div class="collapse" id="help">
                        <div class="card card-body mt-2 mb-2">
                            <p>From here you can add a class, see a high level over view of classes along with the number of students in each class.  <b>Complete</b> classes indicate that the class has been entered at POST
                                and all documentation is complete.  <b>Public</b> classes are those classes that are are visible on the main page to the public.  The classes will fall off the public view after their start date.
                                The <b>barcode icon</b> indicates that the class has the barcode sign-in option turned on.  This allows students the opportunity to scan a barcode with their phone and sign in.  Again this feature
                                is automatically disabled after the start date.  Works great for large classes.</p>
                        </div>
                    </div>
                <table class="table table-hover table-striped mt-2"  id="classes" width="100%" >
                    <thead>
                        <tr>
                            <td class=""><b>Start</b></td>
                            <td class="d-none d-lg-table-cell"><b>Course #</b></td>
                            <td class="d-none d-lg-table-cell" align="center"><b>Hours</b></td>
                            <td><b>Description</b></td>
                            <td><b>Location</b></td>
                            <td class="d-none d-lg-table-cell" align="center"><b>Students</b></td>
                            <td class="d-none d-lg-table-cell" align="center"><b>Complete</b></td>
                            <td align="center"><b>Public</b></td>
                            <td class="d-none d-lg-table-cell" align="center"><i class="fad fa-barcode-read"></i></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($classes as $class)
                        <tr>
                            <td nowrap="" class="">
                                <span class="d-none d-lg-table-cell">{{$class->start_date->format('m-d-Y')}} {{\Carbon\Carbon::createFromFormat('H:i:s',$class->start_time)->format('h:i')}}</span>
                                <span class="d-xl-none">{{$class->start_date->format('m-d-Y')}} </span>
                            </td>
                            <td class="d-none d-lg-table-cell">{{$class->course_id}}</td>
                            <td class="d-none d-lg-table-cell" align="center">{{$class->total_hours}}</td>
                            <td>
                                <span class="d-none d-lg-table-cell">{{$class->class_name}}</span>
                                <span class="d-xl-none">@if(strlen($class->class_name) >25) {{substr($class->class_name, 0, 25) . '...'}} @else {{$class->class_name}} @endif</span>
                            </td>
                            <td>
                                <span class="d-none d-lg-table-cell">{{$class->location}}</span>
                                <span class="d-xl-none">@if(strlen($class->location) >25) {{substr($class->location, 0, 25) . '...'}} @else {{$class->location}} @endif</span>
                            </td>
                            <td class="d-none d-lg-table-cell" align="center">{{$class->studentsClass->count()}}</td>
                            <td class="d-none d-lg-table-cell" align="center">@if($class->completed == 'Yes')<i class="fad fa-check-square" style="color: green"></i> @else <i class="fad fa-times-square" style="color:red"></i> @endif</td>
                            <td align="center">@if($class->class_public == 1) <i class="fad fa-eye"></i> @endif</td>
                            <td class="d-none d-lg-table-cell" align="center">@if($class->barcode_on == 1) <i class="fad fa-check-square" style="color: green"></i> @endif</td>
                            <td align="right"><a href="classes/detail/{{$class->id}}"><i class="fas fa-users-class"></i></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        oTable = $('#classes').DataTable({
            bFilter: true,
            ordering: true,
            aaSorting: [],
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            pageLength: 50,
            dom: 'ltipr'         // This shows just the table
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    </script>
@endsection
