<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scores', function (Blueprint $table) {
            $table->integer('score_percent'); //the human score raw/base
            $table->string('firearm_make')->nullable(); //
            $table->string('firearm_model')->nullable(); //
            $table->string('firearm_serial')->nullable(); //
            $table->text('score_notes')->nullable(); //id number from students
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scores', function (Blueprint $table) {
            $table->dropColumn('studentsnotes')->nullable();
        });
    }
}
