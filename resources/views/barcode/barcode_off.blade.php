@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Barcode Sign-in</b></div>

            <div class="card-body">
                <div class="alert alert-warning">Sorry, the instructor has not enabled barcode sigin-in for this class.  Go tell them to turn it on under the edit class tab.</div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
