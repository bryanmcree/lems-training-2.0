<nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'LEMSt') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="mainNav">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
                </li>
                @can('instructors_only')
                <li class="nav-item">
                    <a class="nav-link" href="/classes">Classes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/students">Students</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://gapost.org" target="_blank">GA POST</a>
                </li>
                @endcan
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <span class="caret"></span>
                            </a>


                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if ( Auth::user()->hasRole('Administrator'))
                                <a class="dropdown-item" href="/agencies"><i class="fad fa-browser"></i> Agencies</a>
                                <a class="dropdown-item" href="/history"><i class="fad fa-history"></i> History</a>
                                <a class="dropdown-item" href="/roles"><i class="fad fa-shield-alt"></i> Security Roles</a>
                                <a class="dropdown-item" href="/users"><i class="fad fa-user-friends"></i> Users</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="https://analytics.google.com/analytics/web/#/report-home/a146443861w208284511p200859687" target="_blank"><i class="fad fa-chart-area"></i> Google Analytics</a>
                                <a class="dropdown-item" href="http://mailgun.com" target="_blank"><i class="fad fa-mailbox"></i> MailGun</a>
                                <a class="dropdown-item" href="http://forge.laravel.com" target="_blank"><i class="fad fa-browser"></i> Forge</a>
                                <a class="dropdown-item" href="http://digitalocean.com" target="_blank"><i class="fad fa-spider-web"></i> DigitalOcean</a>
                            @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fad fa-sign-out-alt"></i> {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
            </ul>
        </div>
    </div>
</nav>