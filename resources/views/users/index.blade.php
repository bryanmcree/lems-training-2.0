@extends('layouts.app')

@section('content')
    @if ( Auth::user()->hasRole('Administrator'))
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>User Management</b></div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a class="navbar-brand" href="#">Users ({{$users->count()}})</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="/users/sendlink">Send Registration Link</a>
                                </li>
                            </ul>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-1" type="search" placeholder="Search" aria-label="Search" id="myInputTextField">
                            </form>
                        </div>
                    </nav>
                <table class="table table-hover table-striped" id="users">
                    <thead>
                        <tr>
                            <td><b>Name</b></td>
                            <td><b>Email</b></td>
                            <td><b>Phone</b></td>
                            <td><b>Type</b></td>
                            <td><b>Created At</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->last_name}}, {{$user->first_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{PhoneNumbers::formatNumber($user->phone)}}</td>
                            <td>{{$user->user_type}}</td>
                            <td>{{$user->created_at}}</td>
                            <td nowrap="" align="right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="/users/{{$user->id}}" class="btn btn-primary"><i class="fad fa-user-edit" title="Edit User"></i></a>
                                    <a href="/users/security/{{$user->id}}" class="btn btn-warning"><i class="fad fa-lock-alt" title="Update Security Roles"></i></a>
                                    <a href="/users/login_as/{{$user->id}}" class="btn btn-secondary"><i class="fad fa-user-secret" title="Login as User"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
        @include('unauthorized')
    @endif
@endsection

@section('scripts')
    <script type="application/javascript">
        oTable = $('#users').DataTable({
            bFilter: true,
            ordering: true,
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            dom: 'ltipr'         // This shows just the table
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    </script>
@endsection


