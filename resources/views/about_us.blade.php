@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>About Us</b></div>

            <div class="card-body">
                LEMSt is an electronic records management system designed for law enforcement instructors and is the product of law enforcement instructors with decades of training experience.
                LEMSt can organize classes maintain student records and is compatible with many portable devices. While LEMSt primary design is around Georgia Law Enforcement we would be open
                to working with other States to provide this service.  Please feel free to contact us.
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
