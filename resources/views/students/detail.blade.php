@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>{{$student->last_name}}, {{$student->first_name}}</b> - Student</div>

            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#edit" role="tab" aria-controls="edit" aria-selected="false"><i class="fas fa-edit"></i> Edit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Classes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-bullseye-arrow"></i> Scores</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="delete-tab" data-toggle="tab" href="#delete" role="tab" aria-controls="delete" aria-selected="false"><i class="fad fa-trash-alt"></i> Delete Student</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card-deck mt-3">
                            <div class="card">
                                <div class="card-header">
                                    <b>Total Classes</b>
                                </div>
                                <div class="card-body">
                                    {{$student->studentsClass->count()}}
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <b>Total Hours</b>
                                </div>
                                <div class="card-body">
                                    {{$student->studentsClass->sum('total_hours')}}
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <b>Avg Score</b>
                                </div>
                                <div class="card-body">
                                    {{number_format($student->scores->avg('score_percent'),0)}}%
                                    <div class="progress">
                                        <div class="progress-bar
                                        @if(number_format($student->scores->avg('score_percent'),0) > 80)
                                        bg-success
                                        @else
                                        bg-warning
                                        @endif

                                            "
                                             role="progressbar" style="width: {{number_format($student->scores->avg('score_percent'),0)}}%"
                                             aria-valuenow="{{number_format($student->scores->avg('score_percent'),0)}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="edit" role="tabpane4" aria-labelledby="edit-tab"><div class="card mt-2">
                            <div class="card-header">
                                <b>Student Details</b>
                            </div>
                            <form method="POST" action="{{ route('update_student') }}">
                                @csrf
                                <input type="hidden" name="student_id" value="{{$student->id}}">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><b>First Name</b></label>
                                        <input type="text" class="form-control" name="first_name" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->first_name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><b>Last Name</b></label>
                                        <input type="text" class="form-control" name="last_name" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->last_name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><b>Suffix</b></label>
                                        <select class="form-control" id="suffix" name="suffix">
                                            <option value="{{$student->suffix}}" selected="selected">{{$student->suffix}}</option>
                                            <option value="Jr.">Jr.</option>
                                            <option value="Sr.">Sr.</option>
                                            <option value="III">III</option>
                                            <option value="IV">IV</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><b>Email</b></label>
                                        <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><b>Phone Number</b></label>
                                        <input type="text" class="form-control" name="phone"  id="phone" maxlength="10" minlength="10" value="{{$student->phone}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><b>Okey</b></label>
                                        <input type="text" class="form-control" name="officer_key_number" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$student->officer_key_number}}">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <input type="submit" value="Update Student" class="btn btn-primary float-right">
                                </div>
                            </form>
                        </div></div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card mt-2">
                            <div class="card-header" >
                                <div class="row" >
                                    <div class="col-md-6">
                                        <b>Classes Participated In</b>
                                    </div>
                                    <div class="col-md-6">
                                        <form class="form-inline float-right">
                                            <div class="form-group">
                                                <input class="form-control input-sm" type="search" placeholder="Search" aria-label="Search" id="myInputTextField">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover table-striped mt-2"  id="classes" width="100%" >
                                    <thead>
                                    <tr>
                                        <td class=""><b>Start</b></td>
                                        <td class="d-none d-lg-table-cell"><b>Course #</b></td>
                                        <td class="d-none d-lg-table-cell" align="center"><b>Hours</b></td>
                                        <td><b>Description</b></td>
                                        <td><b>Location</b></td>
                                        <td class="d-none d-lg-table-cell" align="center"><b>Students</b></td>
                                        <td class="d-none d-lg-table-cell" align="center"><b>Complete</b></td>
                                        <td class="d-none d-lg-table-cell" align="center"><i class="fad fa-barcode-read"></i></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($student->studentsClass as $class)
                                        <tr>
                                            <td nowrap="" class="">
                                                <span class="d-none d-lg-table-cell">{{$class->start_date->format('m-d-Y')}} {{\Carbon\Carbon::createFromFormat('H:i:s',$class->start_time)->format('h:i')}}</span>
                                                <span class="d-xl-none">{{$class->start_date->format('m-d-Y')}} </span>
                                            </td>
                                            <td class="d-none d-lg-table-cell">{{$class->course_id}}</td>
                                            <td class="d-none d-lg-table-cell" align="center">{{$class->total_hours}}</td>
                                            <td>
                                                <span class="d-none d-lg-table-cell">{{$class->class_name}}</span>
                                                <span class="d-xl-none">@if(strlen($class->class_name) >25) {{substr($class->class_name, 0, 25) . '...'}} @else {{$class->class_name}} @endif</span>
                                            </td>
                                            <td>
                                                <span class="d-none d-lg-table-cell">{{$class->location}}</span>
                                                <span class="d-xl-none">@if(strlen($class->location) >25) {{substr($class->location, 0, 25) . '...'}} @else {{$class->location}} @endif</span>
                                            </td>
                                            <td class="d-none d-lg-table-cell" align="center">{{$class->studentsClass->count()}}</td>
                                            <td class="d-none d-lg-table-cell" align="center">@if($class->completed == 'Yes')<i class="fad fa-check-square" style="color: green"></i> @else <i class="fad fa-times-square" style="color:red"></i> @endif</td>
                                            <td class="d-none d-lg-table-cell" align="center">@if($class->barcode_on == 1) <i class="fad fa-check-square" style="color: green"></i> @endif</td>
                                            <td align="right"><a href="classes/detail/{{$class->id}}"><i class="fas fa-users-class"></i></a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                    <div class="tab-pane fade" id="delete" role="tabpanel" aria-labelledby="delete-tab">
                        <div class="card mt-2">
                            <div class="card-body">
                                <p>Deleting a student will remove them from all list.  They will no longer show up in classes they registered for or in search list.  Also the OKey number will still be
                                unique to them and cannot be used again.  This can be undone.</p>
                                <a href="/student/delete/{{$student->id}}" class="btn btn-danger">Delete {{$student->last_name}}, {{$student->first_name}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        oTable = $('#classes').DataTable({
            bFilter: true,
            ordering: true,
            aaSorting: [],
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            dom: 'ltipr'         // This shows just the table
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    </script>
@endsection
