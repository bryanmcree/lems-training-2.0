<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scores extends Model
{
    use SoftDeletes;
    protected $table = 'scores';
    public $incrementing = false;
    protected $guarded = [];

    public function studentScores(){
        return $this->belongsTo('\App\Students','students_id','id');
    }
}
