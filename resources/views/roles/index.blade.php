@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">

            <div class="card-body">
                @if($roles->isEmpty())
                    <div class="alert alert-warning">No Roles Entered</div>
                @else
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a class="navbar-brand" href="#">Roles ({{$roles->count()}})</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="/roles/add">Add Role <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/roles/add">Security Admin <span class="sr-only"></span></a>
                                </li>
                            </ul>
                            <ul class="navbar-nav ">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="collapse" href="#help"><i class="fad fa-walkie-talkie" title="User needs assistance..."></i></a>
                                </li>
                            </ul>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-1" type="search" placeholder="Search" aria-label="Search" id="myInputTextField">
                            </form>
                        </div>
                    </nav>
                    <div class="collapse" id="help">
                        <div class="card card-body mt-2 mb-2">
                            <p>Security roles is a high level administrative function.  Each page has code that regulates what particular users can see and do.  These sections of code are not dynamic and don't change.
                            They are however linked to these roles listed below.  Each role has a particular function and are built on each other.  For example, a user can have administrator privileges but not
                            be able to access the instructor side.  Thus to have true admin access a user would need all the roles.  Adding roles here would not change the function of lems without the hard coding
                            in the background.</p>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <td><b>ID</b></td>
                            <td><b>Name</b></td>
                            <td><b>Description</b></td>
                            <td><b>Created At</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role->id}}</td>
                                <td>{{$role->name}}</td>
                                <td>{{$role->description}}</td>
                                <td>{{$role->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
