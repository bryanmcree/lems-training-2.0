<?php

namespace App\Http\Controllers;

use App\History;
use App\Role;
use App\RoleUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $users = User::all();

        $history = new History();
        $history->function = 'Viewed Users';
        $history->username = \Auth::user()->email;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->save();

        return view('users.index', compact('users'));
    }

    public function edit_user($id){

        $user = User::find($id);
        return view('users.edit', compact('user'));
    }

    public function update_user(){

        $user = User::find(Input::get('user_id'));
        $user->update(Input::except('class_id','student_id'));
        alert()->success('User has been updated!', 'Success!');

        $history = new History();
        $history->function = 'Updated Users';
        $history->username = \Auth::user()->email;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->notes = 'Updated user ' . $user->email;
        $history->save();

        return redirect('/users');
    }

    public function user_security($id){
        $user = User::find($id);
        $current_roles = RoleUser::select('role_id')->where('user_id','=', $id)
            ->get();
        //dd($current_roles);
        $all_roles = Role::wherenotin('id', $current_roles)
            ->orderby('name')
            ->get();
        $assigned_roles = Role::wherein('id', $current_roles)
            ->orderby('name')
            ->get();

        return view('users.security', compact('user','all_roles','current_roles','assigned_roles'));
    }

    public function remove_roles(Request $request){

        $user_id = $request->user_id;

        foreach($request->remove_roles as $role)
        {
            $delete = RoleUser::where('user_id', '=', $request->user_id)
                ->where('role_id', '=', $role)
                ->delete();
        }

        alert()->success('Roles have been deleted!', 'Success!');

        return redirect('/users/security/' . $user_id);
    }

    public function add_roles(Request $request){

        $user_id = $request->user_id;

        foreach($request->add_roles as $role)
        {
            $newRoles = new RoleUser();
            $newRoles->role_id = $role;
            $newRoles->user_id = $request->user_id;
            $newRoles->save();
        }

        alert()->success('Roles Updated!', 'COMPLETE!');
        return redirect('/users/security/' . $user_id);
    }

    public function login_as($id){

        $user = User::where('id','=', $id)->first();
        //dd($user);
        Auth::login($user);
        return redirect('/home');

    }

    public function send_link(){

        return view('users.email_link', compact('user','all_roles','current_roles','assigned_roles'));
    }

    public function send_email(Request $request){

        $input = $request->all();

        $history = new History();
        $history->function = 'Emailed Registration Link';
        $history->username = \Auth::user()->email;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->notes = 'Sent link to ' . $request->email;
        $history->save();

        //Lets send an email to the admin.
        Mail::send('emails.registration_link',array(

            'email' => $request->get('email')), function ($m) use ($input){
            $m->replyTo(\Auth::user()->email);
            $m->to($input['email']);
            $m->subject('LEMSt Wants You!');
        });

        alert()->success('Link has been sent!', 'Success!');
        return redirect('/users/sendlink');

    }
}
