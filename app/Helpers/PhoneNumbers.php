<?php

class PhoneNumbers
{

    public static function formatNumber($phone_number)
    {
        $phone_number = preg_replace("/[^0-9]/", "", $phone_number);

        if (strlen($phone_number) == 7) {
            return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone_number);
        } elseif (strlen($phone_number) == 10) {
            return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone_number);
        } else {
            return $phone_number;
        }

    }

}