<?php

namespace App\Http\Controllers;

use App\History;
use App\Role;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function index(){
        $roles = Role::all();
        return View('roles.index', compact('roles'));
    }

    public function add(){
        return View('roles.add', compact('roles'));
    }

    public function store_role(Request $request){

        $input = $request->all();
        Role::create($input);
        alert()->success('Security role has been added.', 'Role Added');

        $history = new History();
        $history->function = 'Created Role';
        $history->username = \Auth::user()->email;
        $history->notes = 'Created ' . $request->name;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->save();

        return redirect('/roles');
    }
}
