@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header" >
                <div class="row" >
                    <div class="col-md-6">
                        <h2><b>Available Classes</b> ({{$classes->count()}})</h2>
                    </div>
                    <div class="col-md-6">
                        @if(!$classes->isEmpty())
                        <form class="form-inline float-right">
                            <div class="form-group">
                                <input class="form-control input-sm" type="search" placeholder="Search" aria-label="Search" id="myInputTextField">
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body">

                <p>Classes are hosted by instructors thoughout the state of Georgia.  For details regarding attending classes please contact the instructor.  If you are an instructor and want to
                list your classes here please <a href="/contact_us">contact us</a> .
                </p>

                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} </p>
                        @endif
                    @endforeach
                </div>

                @if($classes->isEmpty())
                    <div class="alert alert-warning">There are no classes posted.</div>
                @else
                <table class="table table-hover table-striped mt-2"  id="classes" width="100%" >
                    <thead>
                    <tr>
                        <td class=""><b>Start</b></td>
                        <td class="d-none d-lg-table-cell"><b>Course #</b></td>
                        <td class="d-none d-lg-table-cell" align="center"><b>Hours</b></td>
                        <td><b>Description</b></td>
                        <td><b>Location</b></td>
                        <td align="right"><b>Contact</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($classes as $class)
                        <tr>
                            <td nowrap="" class="">
                                <span class="d-none d-lg-table-cell">{{$class->start_date->format('m-d-Y')}} {{\Carbon\Carbon::createFromFormat('H:i:s',$class->start_time)->format('h:i')}}</span>
                                <span class="d-xl-none">{{$class->start_date->format('m-d-Y')}} </span>
                            </td>
                            <td class="d-none d-lg-table-cell">{{$class->course_id}}</td>
                            <td class="d-none d-lg-table-cell" align="center">{{$class->total_hours}}</td>
                            <td>
                                <span class="d-none d-lg-table-cell">{{$class->class_name}}</span>
                                <span class="d-xl-none">@if(strlen($class->class_name) >25) {{substr($class->class_name, 0, 25) . '...'}} @else {{$class->class_name}} @endif</span>
                            </td>
                            <td>
                                <span class="d-none d-lg-table-cell">{{$class->location}}</span>
                                <span class="d-xl-none">@if(strlen($class->location) >25) {{substr($class->location, 0, 25) . '...'}} @else {{$class->location}} @endif</span>
                            </td>
                            <td align="right"><a href="/contact/instructor/{{$class->id}}"><i class="fad fa-envelope-open-text"></i></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        oTable = $('#classes').DataTable({
            bFilter: true,
            ordering: true,
            aaSorting: [],
            //searching: false,
            bLengthChange: false,
            bInfo: false, //shows records x of x
            bPaginate: true,
            dom: 'ltipr',         // This shows just the table
            language: {
                "emptyTable": "No Public Classes.",
                "zeroRecords": "No classes match."
            }
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    </script>
@endsection
