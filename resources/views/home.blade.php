@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="alert alert-primary alert-dismissible fade show mt-2" role="alert">
            <strong><i class="fad fa-envelope"></i> New Message</strong> - Look for the radio icon <i class="fad fa-walkie-talkie" title="User needs assistance..."></i> next to search blocks for assistance.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="card mt-2">
            <div class="card-header"><b>Dashboard</b></div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-header">
                            <b>Class Volume by Month</b>
                        </div>
                        <div class="card-body">
                            <canvas id="monthly_volums"  ></canvas>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <b>Class Volume by Year</b>
                        </div>
                        <div class="card-body">
                            <canvas id="yearly_volums"  ></canvas>
                        </div>
                    </div>
                    <div class="card" >
                        <div class="card-header">
                            <b>Classes by Title</b>
                        </div>
                        <div class="card-body">
                            <canvas id="classes_type"  ></canvas>
                        </div>
                    </div>
                </div>
                @if($incompleted->isEmpty())
                    <div class="alert alert-primary mt-2" role="alert">
                        You have no classes that need to be completed.
                    </div>
                @else
                <div class="card mt-3">
                    <div class="card-header bg-dark text-white"><b>Incompleted Classes</b></div>
                    <div class="card-body">

                        <table class="table table-hover table-striped mt-2"  id="classes" width="100%" >
                            <thead>
                            <tr>
                                <td class=""><b>Start</b></td>
                                <td class="d-none d-lg-table-cell"><b>Course #</b></td>
                                <td class="d-none d-lg-table-cell" align="center"><b>Hours</b></td>
                                <td><b>Description</b></td>
                                <td><b>Location</b></td>
                                <td class="d-none d-lg-table-cell" align="center"><b>Students</b></td>
                                <td class="d-none d-lg-table-cell" align="center"><b>Complete</b></td>
                                <td class="d-none d-lg-table-cell" align="center"><b>Days Remaining</b></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($incompleted as $class)
                                <tr>
                                    <td nowrap="" class="">
                                        <span class="d-none d-lg-table-cell">{{$class->start_date->format('m-d-Y')}} {{\Carbon\Carbon::createFromFormat('H:i:s',$class->start_time)->format('h:i')}}</span>
                                        <span class="d-xl-none">{{$class->start_date->format('m-d-Y')}} </span>
                                    </td>
                                    <td class="d-none d-lg-table-cell">{{$class->course_id}}</td>
                                    <td class="d-none d-lg-table-cell" align="center">{{$class->total_hours}}</td>
                                    <td>
                                        <span class="d-none d-lg-table-cell">{{$class->class_name}}</span>
                                        <span class="d-xl-none">@if(strlen($class->class_name) >25) {{substr($class->class_name, 0, 25) . '...'}} @else {{$class->class_name}} @endif</span>
                                    </td>
                                    <td>@if(strlen($class->location) >25) {{substr($class->location, 0, 25) . '...'}} @else {{$class->location}} @endif</td>
                                    <td class="d-none d-lg-table-cell" align="center">{{$class->studentsClass->count()}}</td>
                                    <td class="d-none d-lg-table-cell" align="center">
                                        @if($class->completed == 'No')
                                            <a href="/home/complete/{{$class->id}}" class="btn btn-sm btn-outline-danger" title="Mark as Completed?">Not Complete</a>
                                        @endif
                                    <td class="d-none d-lg-table-cell" align="center">{{30-(\Carbon\Carbon::now()->diffInDays($class->start_date))}}</td>
                                    <td align="right"><a href="classes/detail/{{$class->id}}"><i class="fas fa-users-class"></i></a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
                    @if($upcoming->isEmpty())
                    <div class="alert alert-primary mt-2" role="alert">
                        You have no upcoming classes.
                    </div>
                @else
                    <div class="card mt-3">
                        <div class="card-header bg-success text-white"><b>Upcoming Classes</b></div>
                        <div class="card-body">
                            <table class="table table-hover table-striped mt-2"  id="classes" width="100%" >
                                <thead>
                                <tr>
                                    <td class=""><b>Start</b></td>
                                    <td class="d-none d-lg-table-cell"><b>Course #</b></td>
                                    <td class="d-none d-lg-table-cell" align="center"><b>Hours</b></td>
                                    <td><b>Description</b></td>
                                    <td><b>Location</b></td>
                                    <td class="d-none d-lg-table-cell" align="center"><b>Students</b></td>
                                    <td align="center"><b>Public</b></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($upcoming as $class)
                                    <tr>
                                        <td nowrap="" class="">
                                            <span class="d-none d-lg-table-cell">{{$class->start_date->format('m-d-Y')}} {{\Carbon\Carbon::createFromFormat('H:i:s',$class->start_time)->format('h:i')}}</span>
                                            <span class="d-xl-none">{{$class->start_date->format('m-d-Y')}} </span>
                                        </td>
                                        <td class="d-none d-lg-table-cell">{{$class->course_id}}</td>
                                        <td class="d-none d-lg-table-cell" align="center">{{$class->total_hours}}</td>
                                        <td>
                                            <span class="d-none d-lg-table-cell">{{$class->class_name}}</span>
                                            <span class="d-xl-none">@if(strlen($class->class_name) >25) {{substr($class->class_name, 0, 25) . '...'}} @else {{$class->class_name}} @endif</span>
                                        </td>
                                        <td>@if(strlen($class->location) >25) {{substr($class->location, 0, 25) . '...'}} @else {{$class->location}} @endif</td>
                                        <td class="d-none d-lg-table-cell" align="center">{{$class->studentsClass->count()}}</td>
                                        <td align="center">
                                            @if($class->class_public == 0)
                                                <a href="/home/private/{{$class->id}}" class="btn btn-sm btn-outline-dark" title="Make Public?">Private</a>
                                            @else
                                                <a href="/home/private/{{$class->id}}" class="btn btn-sm btn-outline-success" title="Make Private?">Public</a>
                                            @endif
                                        </td>
                                        <td align="right"><a href="classes/detail/{{$class->id}}"><i class="fas fa-users-class"></i></a> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        $(function(){
            $.getJSON("chart/volumes/year", function (result) {
                var labels = [],data=[];
                for (var i = 0; i < result.length; i++) {
                    labels.push(result[i].class_year);
                    data.push(result[i].total);
                }
                var buyerData1 = {
                    labels : labels,
                    datasets: [
                        {
                            label: 'Classes',
                            data: data,
                            backgroundColor: [
                                'rgba(51, 107, 255, .3)',
                                'rgba(95, 50, 255, .3)',
                                'rgba(197, 50, 255, .3)',
                                'rgba(255, 107, 210, .3)',
                                'rgba(255, 50, 108, .3)',
                                'rgba(255, 95, 50, .3)',
                                'rgba(255, 197, 50, .3)',
                                'rgba(108, 255, 50, .3)',
                                'rgba(50, 255, 95, .3)',
                                'rgba(50, 255, 197, .3)',
                                'rgba(50, 210, 255, .3)',
                                'rgba(51, 107, 255, .3)',
                            ],
                            borderColor: "black",
                            borderWidth:1,
                        }
                    ],


                };
                var buyers = document.getElementById('yearly_volums').getContext('2d');
                var chartInstance = new Chart(buyers, {
                    type: 'bar',

                    options: {
                        legend: { display: false },
                        title: {
                            display: true,
                            //text: 'Class Volume by Month'
                        },
                        responsiveAnimationDuration: 6000,
                    },
                    data: buyerData1,


                });
            });

        });



        $(function(){
            $.getJSON("chart/volumes", function (result) {
                var labels = [],data=[];
                for (var i = 0; i < result.length; i++) {
                    labels.push(result[i].month);
                    data.push(result[i].total);
                }
                var buyerData1 = {
                    labels : labels,
                    datasets: [
                        {
                            label: 'Classes',
                            data: data,
                            backgroundColor: [
                                'rgba(51, 107, 255, .3)',
                                'rgba(95, 50, 255, .3)',
                                'rgba(197, 50, 255, .3)',
                                'rgba(255, 107, 210, .3)',
                                'rgba(255, 50, 108, .3)',
                                'rgba(255, 95, 50, .3)',
                                'rgba(255, 197, 50, .3)',
                                'rgba(108, 255, 50, .3)',
                                'rgba(50, 255, 95, .3)',
                                'rgba(50, 255, 197, .3)',
                                'rgba(50, 210, 255, .3)',
                                'rgba(51, 107, 255, .3)',
                            ],
                            borderColor: "black",
                            borderWidth:1,
                        }
                    ],
                };
                var buyers = document.getElementById('monthly_volums').getContext('2d');
                var chartInstance = new Chart(buyers, {
                    type: 'bar',

                    options: {
                        legend: { display: false },
                        title: {
                            display: true,
                            //text: 'Class Volume by Month'
                        },
                        responsiveAnimationDuration: 6000,
                    },
                    data: buyerData1,
                });
            });
        });


        $(function(){
            $.getJSON("chart/volumes/classes", function (result) {
                var labels = [],data=[];
                for (var i = 0; i < result.length; i++) {
                    labels.push(result[i].class);
                    data.push(result[i].total);
                }
                var buyerData1 = {
                    labels : labels,
                    datasets: [
                        {
                            label: 'Classes',
                            data: data,
                            backgroundColor: [
                                'rgba(51, 107, 255, .3)',
                                'rgba(95, 50, 255, .3)',
                                'rgba(197, 50, 255, .3)',
                                'rgba(255, 107, 210, .3)',
                                'rgba(255, 50, 108, .3)',
                                'rgba(255, 95, 50, .3)',
                                'rgba(255, 197, 50, .3)',
                                'rgba(108, 255, 50, .3)',
                                'rgba(50, 255, 95, .3)',
                                'rgba(50, 255, 197, .3)',
                                'rgba(50, 210, 255, .3)',
                                'rgba(51, 107, 255, .3)',
                                'rgba(51, 107, 255, .3)',
                                'rgba(95, 50, 255, .3)',
                                'rgba(197, 50, 255, .3)',
                                'rgba(255, 107, 210, .3)',
                                'rgba(255, 50, 108, .3)',
                                'rgba(255, 95, 50, .3)',
                                'rgba(255, 197, 50, .3)',
                                'rgba(108, 255, 50, .3)',
                                'rgba(50, 255, 95, .3)',
                                'rgba(50, 255, 197, .3)',
                                'rgba(50, 210, 255, .3)',
                                'rgba(51, 107, 255, .3)',
                                'rgba(51, 107, 255, .3)',
                                'rgba(95, 50, 255, .3)',
                                'rgba(197, 50, 255, .3)',
                                'rgba(255, 107, 210, .3)',
                                'rgba(255, 50, 108, .3)',
                                'rgba(255, 95, 50, .3)',
                                'rgba(255, 197, 50, .3)',
                                'rgba(108, 255, 50, .3)',
                                'rgba(50, 255, 95, .3)',
                                'rgba(50, 255, 197, .3)',
                                'rgba(50, 210, 255, .3)',
                                'rgba(51, 107, 255, .3)',
                            ],
                            borderColor: "black",
                            borderWidth:1,
                        }
                    ],
                };
                var buyers = document.getElementById('classes_type').getContext('2d');
                var chartInstance = new Chart(buyers, {
                    type: 'horizontalBar',

                    options: {
                        legend: { display: false },
                        title: {
                            display: true,
                            //text: 'Class Volume by Month'
                        },
                        responsiveAnimationDuration: 6000,
                    },
                    data: buyerData1,
                });
            });
        });

    </script>
@endsection
