@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Add Scores</b></div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                    <form method="POST" action="{{ route('new_score') }}">
                        @csrf
                        <input type="hidden" name="class_id" value="{{$class->id}}">
                        <input type="submit" class="btn btn-primary float-right" value="Input Score">
                        <a href="/classes/detail/{{$class->id}}" class="btn btn-secondary ">Cancel and Return to Class</a>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><b>Select Student</b></label>
                        <select name="students_id" class="form-control students" required>
                            <option></option>
                            @foreach ($class->studentsClass as $students)
                            <option value="{{$students->id}}">{{$students->last_name}}, {{$students->first_name}} - {{$students->officer_key_number}}</option>
                            @endforeach()
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Base Score</b></label>
                        <input type="number" class="form-control" name="base_score" value="300" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Raw Score</b></label>
                        <input type="number" class="form-control" name="raw_score" placeholder="Input Raw Score" required>
                    </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect2"><b>Score Type</b></label>
                            <select class="form-control" name="type" required>
                                <option value="Firearms" selected>Firearms</option>
                                <option value="Written Test">Written Test</option>
                                <option value="Practical">Practical</option>
                            </select>
                        </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><b>Pass or Fail</b></label>
                        <select class="form-control" name="fail">
                            <option value="Pass" selected>Pass</option>
                            <option value="Fail">Fail</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect2"><b>Firearms Course</b></label>
                        <select class="form-control" name="course">
                            <option value="N/A" selected>N/A</option>
                            <option value="Pistol">Pistol</option>
                            <option value="Shotgun">Shotgun</option>
                            <option value="Sub Gun">Sub Gun</option>
                            <option value="Rifle">Rifle</option>
                            <option value="Precision Rifle">Precision Rifle</option>
                            <option value="Backup">Backup</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Firearm Make</b></label>
                        <input type="text" class="form-control" name="firearm_make" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Firearm Model</b></label>
                        <input type="text" class="form-control" name="firearm_model" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Firearm Serial Number</b></label>
                        <input type="text" class="form-control" name="firearm_serial" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Score Notes</b></label>
                        <textarea class="form-control" name="score_notes" rows="3"></textarea>
                    </div>
                        <input type="submit" class="btn btn-primary float-right" value="Input Score">
                        <a href="/classes/detail/{{$class->id}}" class="btn btn-secondary ">Cancel and Return to Class</a>
                    </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

        $(document).ready(function() {
            $('.students').select2({
                placeholder: "Select student",
                allowClear: true
            });
        });

    </script>
@endsection
