<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index(){

        $history_log = History::orderby('created_at','DESC')
            ->paginate(50);

        $history = new History();
        $history->function = 'Viewed History';
        $history->username = \Auth::user()->email;
        $history->ip_address = \Illuminate\Support\Facades\Request::getClientIp();
        $history->save();

        return View('history.index', compact('history_log'));
    }
}
