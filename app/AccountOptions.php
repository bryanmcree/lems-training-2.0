<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountOptions extends Model
{
    protected $table = 'account_options';
    protected $guarded = [];


    public function sharedStudents(){
        return $this->hasMany('\App\Students','account_number','account_number');
    }
}
