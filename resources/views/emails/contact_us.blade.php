
<div style="font-family: sans-serif;">
    <table width="100%" align="left" style="border-spacing: 0;border-collapse: collapse;">
        <tr>
            <td colspan="2" style="padding: 0;" bgcolor="#2e9e7b"><h2 style="font-family: sans-serif; color: white">You have a new message!</h2></td>
        </tr>
        <tr>
            <td width="50%" style="padding: 0;" align="right"><b>Name:</b></td>
            <td>{{$name}}</td>
        </tr>
        <tr>
            <td width="50%" style="padding: 0;" align="right"><b>Phone:</b></td>
            <td>{{$phone_number}}</td>
        </tr>
        <tr>
            <td width="50%" style="padding: 0;" align="right"><b>Email Address:</b></td>
            <td>{{$email}}</td>
        </tr>
        <tr>
            <td width="50%" style="padding: 0;" align="right"><b>Ip Address:</b></td>
            <td>{{$ip_address}}</td>
        </tr>
        <tr>
            <td width="50%" style="padding: 0;" align="right"><b>GA POST Instructor wanting Access:</b></td>
            <td>{{$ga_post}}</td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 0;" align="center"><hr></td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 0;" align="center"><b>Comments</b></td>
        </tr>
        <tr>
            <td colspan="2">{{$comments}}</td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#d3d3d3"><p>Copyright &copy; lemstweb.com - {{Carbon\Carbon::now()->format('Y')}}</p></td>
        </tr>
    </table>
</div>