<?php

namespace App\Http\Controllers;

use App\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StudentsController extends Controller
{


    public function searchStudents(Request $request)
    {
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $tags = Students::select('first_name','last_name','id','officer_key_number','suffix')
            ->where('first_name', 'LIKE', '%' . $term . '%')
            ->orwhere('last_name', 'LIKE', '%' . $term . '%')
            ->orwhere('officer_key_number', 'LIKE', '%' . $term . '%')
            ->limit(5)
            ->distinct()
            ->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->last_name .', ' . $tag->first_name . ' ' . $tag->suffix . ' - ' . $tag->officer_key_number];
        }

        return \Response::json($formatted_tags);
    }

    public function index(){
        $students = Students::orderby('last_name')
            ->get();

        //dd($students);

        return view('students.index', compact('students'));
    }

    public function edit_student($id){

        $student = Students::where('id','=', $id)
            ->first();

        return view('students.edit', compact('student'));

    }

    public function create_student(){

        return view('students.add_student');
    }

    public function add_student(Request $request){

        //make sure okey is unique

        $validatedData = $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'officer_key_number' => ['required', 'string', 'max:255', 'unique:students'],
        ]);

        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('account_number', Auth::user()->account_number);
        $input = $request->all();
        Students::create($input);
        alert()->success('Student has been added.', 'Student Added');
        return redirect('/students');
    }

    public function detail($id){

        $student = Students::find($id);

        return view('students.detail', compact('student'));
    }

    public function delete_student($id)
    {
        Students::find($id)->delete();
        alert()->success('Student has been deleted.', 'Student Deleted');
        return redirect('/students');
    }
}
