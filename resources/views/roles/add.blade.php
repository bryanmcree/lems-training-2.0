@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Add Role</b></div>

            <div class="card-body">
                <form method="POST" action="{{ route('store_role') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Role Name</b></label>
                        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Role Description</b></label>
                        <input type="text" name="description" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="Add Role">
                    <input type="reset" class="btn btn-secondary">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
