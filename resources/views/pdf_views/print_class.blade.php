@extends('layouts.public')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>{{$data->course_id}}-{{$data->class_name}} ({{$data->total_hours}} hours)</b>
                <div class="card-subtitle mb-2 text-muted"><i>{{$data->location}}</i></div>
            </div>

            <div class="card-body">
                {{$data->studentsnotes}}
            </div>
            <div class="card-body">
                <table class="table table-hover table-striped" id="students" width="100%">
                    <thead>
                    <tr>
                        <td class="d-none d-sm-table-cell"><b>Okey</b></td>
                        <td><b>Name</b></td>
                        <td class="d-none d-sm-table-cell" align="center"><b>Signature</b></td>
                        <td class="d-none d-sm-table-cell" align="center"><b>Signed On</b></td>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data->studentsClass as $students)
                        <tr>
                            <td class="d-none d-sm-table-cell">{{$students->officer_key_number}}</td>
                            <td>{{$students->last_name}}, {{$students->first_name}}</td>
                            <td class="d-none d-sm-table-cell" align="center">@if($students->pivot->signature == NULL)No Signature
                                @else <img  src='data:image/svg+xml; {{$students->pivot->signature}}' style="height: 30px;"> @endif</td>
                            <td class="d-none d-sm-table-cell" align="center">@if(is_null($students->pivot->signature_date)) No Date @else {{\Carbon\Carbon::parse($students->pivot->signature_date)->format('m-d-Y H:i')}} @endif</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-body">

                @if($data->studentScores->where('class_id', $data->id)->isEmpty())
                    <div class="alert alert-info">No scores for this class.</div>
                @else
                    <table class="documents table table-condensed table-hover table-bordered">
                        <thead>
                        <tr>
                            <td><b>Student Name</b>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($data->studentsClass as $students)
                            <tr>
                                <td><b>{{$students->last_name}}, {{$students->first_name}}</b></td>
                                @foreach ($students->scores->where('class_id', $data->id) as $scores)
                                    <td nowrap=""><a href="/scores/edit/{{$scores->id}}?C={{$data->id}}" class="btn btn-block @if($scores->fail =='Fail') btn-danger @else btn-success @endif btn-sm">
                                            @if($scores->type == 'Firearms')<i class="far fa-bullseye"></i>
                                            @elseif($scores->type == 'Written Test')W
                                            @elseif($scores->type == 'Practical')P
                                            @endif{{$scores->score_percent}}%
                                               -  {{$scores->course}}
                                        </a>
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
            <div class="card-footer">
                <span>Print Class - {{Carbon\Carbon::now()}}</span>
                <span class="float-right">LEMSt - lemstweb.com</span>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
