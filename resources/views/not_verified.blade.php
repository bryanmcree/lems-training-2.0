
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Verification Pending</b></div>

            <div class="card-body">
                <p>LEMSt is still processing your registration.  After that is done you will be notified via email.</p>
            </div>
        </div>
    </div>

