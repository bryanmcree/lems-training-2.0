@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mt-2">
            <div class="card-header"><b>Update Security - {{$user->last_name}}, {{$user->first_name}}</b>
            <a href="/users" class="btn btn-sm btn-secondary float-right">Return to Users</a>
            </div>
            <div class="card-body">
                <div class="card mt-2">
                    <div class="card-header"><b>Assigned Roles</b></div>
                    <div class="card-body">
                        @if($assigned_roles->isEmpty())
                            <div class="alert alert-warning">No roles assigned</div>
                        @else
                            <form method="POST" action="{{ route('remove_roles') }}">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                @foreach($assigned_roles as $role)
                                    <div class="form-check">
                                        <input class="form-check-input" name="remove_roles[]" type="checkbox" value="{{$role->id}}" id="{{$role->id}}">
                                        <label class="form-check-label" for="{{$role->id}}">
                                            <b>{{$role->name}}</b> - <i>{{$role->description}}</i>
                                        </label>
                                    </div>
                                @endforeach
                                    <input type="submit" class="btn btn-danger mt-2" value="REMOVE Above Roles">
                            </form>

                        @endif
                    </div>
                </div>
                <div class="card mt-2">
                    <div class="card-header"><b>Available Roles</b></div>
                    <div class="card-body">
                        @if($all_roles->isEmpty())
                            <div class="alert alert-warning">No roles assigned</div>
                        @else
                            <form method="POST" action="{{ route('add_roles') }}">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                @foreach($all_roles as $role)
                                    <div class="form-check">
                                        <input class="form-check-input" name="add_roles[]" type="checkbox" value="{{$role->id}}" id="{{$role->id}}">
                                        <label class="form-check-label" for="{{$role->id}}">
                                            <b>{{$role->name}}</b> - <i>{{$role->description}}</i>
                                        </label>
                                    </div>
                                @endforeach
                                <input type="submit" class="btn btn-primary mt-2" value="Add Above Roles">
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">

    </script>
@endsection
